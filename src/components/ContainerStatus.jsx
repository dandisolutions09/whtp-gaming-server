// const ContainerStatus = ({ container }) => {
//     const { containerId, name, status } = container;

//     return (
//       <div className="container-status">
//         {/* Render the container details */}
//         <p>Container ID: {containerId}</p>
//         <p>Name: {name}</p>

//         {/* Render the status */}
//         <p>Status: {status}</p>

//         {/* Conditional rendering based on the name */}
//         {name.includes("Minecraft") && (
//           <div className="absolute top-0 right-0 text-xs m-2">
//             stopped...
//           </div>
//         )}
//       </div>
//     );
//   };

//   export ContainerStatus

const ContainerStatus = ({ container }) => {
  const { containerId, name, status } = container;
  return (
    <div className="container-status">
      {/* Render the container details */}
      <p>Container ID: {containerId}</p>
      <p>Name: {name}</p>

      {/* Render the status */}
      <p>Status: {status}</p>

      {/* Conditional rendering based on the name */}
      {name.includes("Minecraft") && (
        <div className="absolute top-0 right-0 text-xs m-2">stopped...</div>
      )}
    </div>
  );
};

export default ContainerStatus;
