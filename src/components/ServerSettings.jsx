import {
  Box,
  Button,
  CircularProgress,
  Modal,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState, useEffect, useRef } from "react";
import { useFormik } from "formik";

import rust from "../assets/rust.jpg";
import endpoints from "../endpoints";
import axios from "axios";
import GeneralError from "../Alerts/GeneralError";

const ServerSettings = () => {
  const [logs, setLogs] = useState([]);
  const [stats, setStats] = useState("");

  const [memUsage, setMemUsage] = useState("");
  const [cpuUsage, setCPUUsage] = useState("");
  const [players, setPlayers] = useState("");

  const [containerID, setContainerID] = useState(
    "fc3c7563774f90d1fd1266aaf7b31ffa02ab1dee8ad10b5f25c179bbffb0261f"
  ); // Default container ID

  const [gameNameGlobal, setGameNameGlobal] = useState();

  const [settingsModal, setOpenSettingsModal] = useState(false);

  const [loading, setLoading] = useState();

  //setOpenSettingsModal
  const logsEndRef = useRef(null); // Reference for auto-scrolling

  const user = localStorage.getItem("user-id");

  const user_id = user._id;

  const formatMessages = (message) => {
    // Use a regular expression to extract CPU and Memory usage percentages
    const cpuMatch = message.match(/CPU Usage:\s+([\d.]+)%/);
    const memoryMatch = message.match(
      /Memory Usage:\s*[\d.]+\sGB\s\/\s[\d.]+\sGB\s\(([\d.]+)%\)/
    );

    // Extract the values if they were found
    const cpuUsage = cpuMatch ? cpuMatch[1] : "N/A"; // Default to 'N/A' if not found
    const memoryUsage = memoryMatch ? memoryMatch[1] : "N/A"; // Default to 'N/A' if not found

    return {
      cpuUsage: `${cpuUsage}%`,
      memoryUsage: `${memoryUsage}%`,
    };
  };

  useEffect(() => {
    //        const eventSource = new EventSource(`http://localhost:8081/sse/docker-stats?container_id=${containerId}`); // Adjust URL as needed

    // const eventSource = new EventSource(
    //   "http://localhost:8081/sse/docker-stats"
    // );

    // const user = localStorage.getItem("user-id");

    setLoading(true);

    const user_id = localStorage.getItem("user-id");
    console.log("user ID", user_id);

    const game_name = localStorage.getItem("game_name_for_settings");

    console.log("received game name", game_name);

    //setGameNameGlobal(game_name);

    const user = JSON.parse(localStorage.getItem("user"));

    // const user_id = user._id;

    // const eventSource = new EventSource(
    //   //`${endpoints.getContainerStats}?user_id=${user_id}&game_name=Minecraft`

    //   `${endpoints.getResourcesUsage}?user_id=${user_id}&game_name=${game_name}`
    // );

    // eventSource.onmessage = (event) => {
    //   // Update the stats with new data
    //   console.log("console", event.data);

    //   const parts = event.data.split(": ");
    //   if (parts.length !== 2) return; // skip if the log is not properly formatted

    //   // Extract container ID and name
    //   const containerIDAndName = parts[0].trim();
    //   const usageAndTotal = parts[1].trim();

    //   // Split CPU and memory usage
    //   const usageParts = usageAndTotal.split(" - ");
    //   if (usageParts.length !== 2) return; // skip if the usage part is not properly formatted

    //   const cpuUsageStr = usageParts[0].trim().replace("%", ""); // Remove the % sign
    //   const memUsage = usageParts[1];

    //   //console.log("cpu usage", cpuUsageStr);
    //   console.log("event", event.data);

    //   setCPUUsage(cpuUsageStr);

    //   // console.log("mem usage usage", memUsage);
    //   setMemUsage(memUsage);

    //   setPlayers("9");

    //   //console.log("container and name", containerIDAndName);

    //   //console.log("mem usage", memUsage);

    //   // Convert CPU usage to a float
    //   const cpuUsage = parseFloat(cpuUsageStr);
    //   setStats((prevStats) => prevStats + event.data + "\n");
    // };

    // eventSource.onerror = (error) => {
    //   console.error("EventSource failed:", error);
    //   eventSource.close(); // Close the connection on error
    // };

    // Create WebSocket connection

    // `${endpoints.getContainerStats}?user_id=${user_id}&game_name=Minecraft`

    //websocket

    // const websocketUrl = `ws://192.168.100.71:8081/logs?user_id=${user_id}&game_name=${game_name}`;
    const websocketUrl = `ws://localhost:8081/logs?user_id=${user_id}&game_name=${game_name}`;


    //const websocket = new WebSocket("ws://localhost:8081/logs");
    const websocket = new WebSocket(websocketUrl);

    websocket.onopen = () => {
      console.log("WebSocket connection opened");
    };

    websocket.onmessage = (event) => {
      // console.log("WebSocket message:", event.data);

      console.log("Received logs:", event.data);
      setLogs((prevLogs) => [...prevLogs, event.data]);
      //setLogs(event.data);
      // Handle the WebSocket message similarly to EventSource
      // For example, if it follows the same format:
      // const parts = event.data.split(": ");
      // if (parts.length !== 2) return;

      // const containerIDAndName = parts[0].trim();
      // const usageAndTotal = parts[1].trim();
      // const usageParts = usageAndTotal.split(" - ");
      // if (usageParts.length !== 2) return;

      // const cpuUsageStr = usageParts[0].trim().replace("%", "");
      // const memUsage = usageParts[1];

      // console.log("WebSocket CPU Usage:", cpuUsageStr);
      // setCPUUsage(cpuUsageStr);
      // console.log("WebSocket Memory Usage:", memUsage);
      // setMemUsage(memUsage);
    };

    websocket.onerror = (error) => {
      console.error("WebSocket error:", error);
      websocket.close();
    };

    websocket.onclose = () => {
      console.log("WebSocket connection closed");
    };

    /// second SSE for docker containers

    // const dockerContainer_events = new EventSource(
    //   //`${endpoints.getContainerStats}?user_id=${user_id}&game_name=Minecraft`

    //   `${endpoints.getResourcesUsage}?user_id=${user_id}&game_name=Minecraft`
    // );

    // dockerContainer_events.onmessage = (event) => {
    //   // Update the stats with new data
    //   console.log("docker container", event.data);

    //   const parts = event.data.split("| ");
    //   if (parts.length !== 2) return; // skip if the log is not properly formatted

    //   // Extract container ID and name
    //   const containerIDAndName = parts[0].trim();
    //   const usageAndTotal = parts[1].trim();

    //   // Split CPU and memory usage
    //   const usageParts = usageAndTotal.split(" - ");
    //   if (usageParts.length !== 2) return; // skip if the usage part is not properly formatted

    //   const cpuUsageStr = usageParts[0].trim().replace("%", ""); // Remove the % sign
    //   const memUsage = usageParts[1];

    //   console.log("cpu usage", cpuUsageStr);

    //   setCPUUsage(cpuUsageStr);

    //   // console.log("mem usage usage", memUsage);
    //   setMemUsage(memUsage);

    //   setPlayers("9");

    //   console.log("container and name", containerIDAndName);

    //   console.log("mem usage", memUsage);

    //   //Convert CPU usage to a float
    //   const cpuUsage = parseFloat(cpuUsageStr);
    //   setStats((prevStats) => prevStats + event.data + "\n");
    // };

    // dockerContainer_events.onerror = (error) => {
    //   console.error("EventSource failed:", error);
    //   eventSource.close(); // Close the connection on error
    // };

    const eventSource = new EventSource(
      //`${endpoints.getContainerStats}?user_id=${user_id}&game_name=Minecraft`

      `${endpoints.getContainerResourceUsage}?user_id=${user_id}&game_name=Minecraft`
    );

    // Listen for messages from the server
    eventSource.onmessage = (event) => {
      console.log("events data", event.data);

      const formattedData = formatMessages(event.data);
      console.log("formatted:--", formattedData.cpuUsage); // { cpuUsage: "0.74%", memoryUsage: "5.47%" }
      setCPUUsage(formattedData.cpuUsage);
      setMemUsage(formattedData.memoryUsage);

      setLoading(false);

      //const formattedData = formatContainerStats(event.data, user_id);
      // console.log("formattedData", formattedData);
      // setMessages(formattedData);
    };

    // Handle errors
    eventSource.onerror = (error) => {
      console.error("EventSource failed:", error);
      eventSource.close();
    };

    // Cleanup on component unmount
    return () => {
      eventSource.close();
      //dockerContainer_events.close();
      websocket.close();
    };
  }, []);

  // useEffect(() => {
  //   // Create WebSocket connection
  //   const socket = new WebSocket("ws://localhost:8081/stats");

  //   // Connection opened
  //   socket.onopen = () => {
  //     console.log("WebSocket connection established");
  //   };

  //   // Listen for messages
  //   socket.onmessage = (event) => {
  //     console.log("Received logs:", event.data);
  //     setLogs((prevLogs) => [...prevLogs, event.data]);
  //   };

  //   // Handle WebSocket error
  //   socket.onerror = (error) => {
  //     console.error("WebSocket error:", error);
  //   };

  //   // Handle WebSocket close
  //   socket.onclose = (event) => {
  //     console.log("WebSocket connection closed:", event);
  //   };

  //   // Clean up WebSocket connection when the component unmounts
  //   return () => {
  //     socket.close();
  //   };
  // }, [containerID]);

  // Function to scroll to the bottom of the logs
  const scrollToBottom = () => {
    logsEndRef.current?.scrollIntoView({ behavior: "smooth" });
  };

  const handleOpenModal = () => {
    //logsEndRef.current?.scrollIntoView({ behavior: "smooth" });
    setOpenSettingsModal(true);
  };

  const handleCloseModal = () => {
    //logsEndRef.current?.scrollIntoView({ behavior: "smooth" });
    setOpenSettingsModal(false);
  };

  const handleStartServer = () => {
    //logsEndRef.current?.scrollIntoView({ behavior: "smooth" });
    //setOpenSettingsModal(false);
    // Make the POST request
    //`${endpoints.getAdminById}/${stored_json_data.admin_id}`

    // const data = {
    //   game: "rust",
    // };
    const userID = localStorage.getItem("user-id");
    console.log("user id", userID);

    const game_name = "rust";
    axios
      .post(`${endpoints.starServer}?game=${game_name}&uid=${userID}`)
      .then((res) => {
        console.log(res.data);
        //setResponse(res.data); // Save response data
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };

  const postData = async (data_) => {
    // let installment_array = [];
    console.log("data to be sent", data_);
    // records_array.push(data_);

    const user_id = localStorage.getItem("user-id");
    console.log("user ID", user_id);

    //console.log("new incoming installments", incoming_instalments);

    // var storedDataJSON = localStorage.getItem("userData_storage");
    // var storedData = JSON.parse(storedDataJSON);

    // const response = await axios.get(
    //   `${endpoints.starServer}?user_id=${user}`
    // );

    try {
      const response = await fetch(
        `${endpoints.postRustServerConfig}?user_id=${user}`,

        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data_),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
        // setOpenRecordAddedSuccessfully(true);

        //setOpenRecordChangeSuccessfully(true);
        // setRefresher(true);

        // setTimeout(() => {
        //   setOpenBooking(false);
        //   reloadPage();
        // }, 2000);
        //setOpenRecordAddedSuccessfully(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  // Use effect to auto-scroll when logs are updated
  useEffect(() => {
    // scrollToBottom();
  }, [logs]);

  const formikMain = useFormik({
    initialValues: {
      server_name: "",
      // server_banner: "",
      // server_identity: "",
      server_description: "",
      max_players: "",
      world_size: "",
      seed: "",

      // client_email: "",`t
      // client_nat_id: "",
    },

    onSubmit: (values) => {
      console.log("values", values);
      postData(values);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });

  return (
    <>
      {/* {loading && (
        <div>
          <CircularProgress color="success" />
        </div>
      )} */}
      <Modal open={settingsModal} onClose={handleCloseModal}>
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <Typography
              id="modal-modal-title"
              sx={{
                fontWeight: "bold",
                fontSize: ["14px", "18px"],
                color: "black",
              }}
            >
              Rust Server Configuration
            </Typography>
            {/* <h2 className="font-bold m-4"> Add Record </h2> */}
            {/* <CloseIcon
            fontSize="40"
            onClick={handleClostRustModal}
            sx={{ cursor: "pointer" }}
          /> */}
          </div>
          <form onSubmit={formikMain.handleSubmit}>
            <div className="flex flex-col gap-4 ">
              <div className="flex flex-col gap-3">
                <Typography
                  id="modal-modal-title"
                  sx={{ fontWeight: "bold", fontSize: ["14px", "16px"] }}
                >
                  Client's Details
                </Typography>
                <div className="flex flex-col gap-3">
                  <div className="flex flex-row gap-3 mt-3">
                    <TextField
                      type="text"
                      id="server_name"
                      name="server_name"
                      label="Rust Server Name"
                      placeholder="Your Rust Server Name"
                      value={formikMain.values.server_name}
                      error={
                        formikMain.touched.server_name &&
                        Boolean(formikMain.errors.server_name)
                      }
                      helperText={
                        formikMain.touched.server_name &&
                        formikMain.errors.server_name
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    />

                    {/* <TextField
                      type="text"
                      id="server_banner"
                      name="server_banner"
                      label="Server Banner"
                      placeholder="Server Banner"
                      value={formikMain.values.server_banner}
                      error={
                        formikMain.touched.server_banner &&
                        Boolean(formikMain.errors.server_banner)
                      }
                      helperText={
                        formikMain.touched.server_banner &&
                        formikMain.errors.server_banner
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      //onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    /> */}

                    {/* <TextField
                      type="text"
                      id="server_identity"
                      name="server_identity"
                      label="Server Identity"
                      placeholder="Server Identity"
                      value={formikMain.values.server_identity}
                      error={
                        formikMain.touched.server_identity &&
                        Boolean(formikMain.errors.server_identity)
                      }
                      helperText={
                        formikMain.touched.server_identity &&
                        formikMain.errors.server_identity
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      //onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    /> */}
                  </div>

                  <TextField
                    type="text"
                    id="server_description"
                    name="server_description"
                    label="Server Description"
                    placeholder="Server Description"
                    value={formikMain.values.server_description}
                    error={
                      formikMain.touched.server_description &&
                      Boolean(formikMain.errors.server_description)
                    }
                    helperText={
                      formikMain.touched.server_description &&
                      formikMain.errors.server_description
                    }
                    onChange={(e) => {
                      e.target.value = e.target.value.toUpperCase();
                      formikMain.handleChange(e);
                    }}
                    //onBlur={formikMain.handleBlur}
                    // sx={{
                    //   width: ["100%", "50%"], // Adjust width for different screen sizes
                    //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                    // }}
                  />

                  <div className="flex flex-row gap-3 ">
                    {/* <TextField
                      type="text"
                      id="max_players"
                      name="max_players"
                      label="Max Players"
                      placeholder="Max Players"
                      value={formikMain.values.max_players}
                      error={
                        formikMain.touched.max_players &&
                        Boolean(formikMain.errors.max_players)
                      }
                      helperText={
                        formikMain.touched.max_players &&
                        formikMain.errors.max_players
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      //onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    /> */}

                    <TextField
                      type="text"
                      id="world_size"
                      name="world_size"
                      label="World Size"
                      placeholder="World Size"
                      value={formikMain.values.world_size}
                      error={
                        formikMain.touched.world_size &&
                        Boolean(formikMain.errors.world_size)
                      }
                      helperText={
                        formikMain.touched.world_size &&
                        formikMain.errors.world_size
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      //onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    />

                    <TextField
                      type="text"
                      id="seed"
                      name="seed"
                      label="Server Seed"
                      placeholder="Server Seed"
                      value={formikMain.values.seed}
                      error={
                        formikMain.touched.seed &&
                        Boolean(formikMain.errors.seed)
                      }
                      helperText={
                        formikMain.touched.seed && formikMain.errors.seed
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      //onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    />
                    {/* <TextField
                      type="text"
                      id="client_lastname"
                      name="client_lastname"
                      label="Last Name"
                      placeholder="Last Name"
                      value={formikMain.values.client_lastname}
                      error={
                        formikMain.touched.client_lastname &&
                        Boolean(formikMain.errors.client_lastname)
                      }
                      helperText={
                        formikMain.touched.client_lastname &&
                        formikMain.errors.client_lastname
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    /> */}
                  </div>
                </div>
              </div>
            </div>

            <div className="flex flex-row gap-3 mt-">
              <div className="flex flex-row gap-3 mt-3"></div>
            </div>

            <Typography
              id="modal-modal-title"
              sx={{ fontWeight: "bold", marginTop: 2 }}
            >
              Agent's Information
            </Typography>
            {/* <div className="flex text-center justify-center mt-4 ">
              <Box className="flex flex-row bg-gray-100 w-[550px]  rounded-lg shadow-xl gap-[300px]">
                <Search>
                  <SearchIconWrapper>
                    <SearchIcon />
                  </SearchIconWrapper>
                  <StyledInputBase
                    //onClick={handleSearchModal}
                    //onClick={aboutToSearch}
                    placeholder="Type Agent"
                    // placeholder={searchType || "Search Name..."}
                    inputProps={{ "aria-label": "search" }}
                    value={searchValue}
                    onChange={(e) => handleChange(e.target.value)}
                  />
                </Search>
     
              </Box>
            </div> */}

            {/* <Button type="submit">Save Agent</Button> */}
            {/* </form> */}

            <div className="flex m-4 gap-4 ">
              <Button
                variant="contained"
                size="large"
                type="submit"
                sx={{ backgroundColor: "#242333" }}
                // onClick={handle_submit}
              >
                Save Record
              </Button>
              {/* <Button
              onClick={handleClostRustModal}
              variant="contained"
              sx={{ backgroundColor: "#242333" }}
              size="large"
            >
              Close
            </Button> */}
            </div>
          </form>
        </Box>
      </Modal>
      <div>
        {/* Dashboard Title */}

        <div className="bg-gray-900 text-white p-6 rounded-lg shadow-lg w-full mb-6">
          <h1 className="text-4xl font-bold mb-4 text-white-400">Dashboard</h1>
          {/* Notification Bar */}
          <div className="bg-gray-800 p-4 mb-4 flex items-center justify-between rounded-lg">
            <span className="text-yellow-400">
              ⚠️ Your rental time will be over in 3 days. Extend your rental
              time{" "}
              <a href="#" className="text-yellow-500 underline">
                now
              </a>
              .
            </span>
          </div>

          {/* Welcome Text */}
          <p className="mb-4">
            Welcome to your personal server! Here you can find important overall
            information about your server. On the left-side panel, you will find
            more options to customize and manage your server.
          </p>

          {/* Action Buttons */}
          {/* <div className="space-x-4">
            <button
              className="bg-green-600 hover:bg-green-500 text-white py-2 px-4 rounded-lg"
              onClick={handleStartServer}
            >
              🔄 Start server
            </button>
            <button className="bg-red-600 hover:bg-red-500 text-white py-2 px-4 rounded-lg">
              🛑 Stop server
            </button>
            <button
              className="bg-green-600 hover:bg-green-500 text-white py-2 px-4 rounded-lg"
              onClick={handleOpenModal}
            >
              🔄 Modify Settings
            </button>
          </div> */}

          <button
            className="bg-green-600 hover:bg-green-500 text-white py-2 px-4 rounded-lg"
            onClick={handleOpenModal}
          >
            🔄 Modify Settings
          </button>
        </div>

        <div className="flex flex-row gap-6">
          {/* Server Status Section */}
          <div className="bg-gray-900 text-white p-6 rounded-lg shadow-lg w-1/2 flex items-center">
            {/* Left section - Image */}
            <div className="w-1/2 mr-8">
              <img
                src={rust}
                alt="Server Status"
                className="rounded-lg w-full"
              />
            </div>

            {/* Right section - Server details */}
            <div className="w-1/4 pl-4">
              <h2 className="text-2xl font-bold mb-4">Server Status</h2>

              <div className="mb-2">
                <strong>Servername: </strong>
                <span className="text-yellow-500">{status.serverName}</span>
              </div>

              <div className="mb-2">
                <strong>IP Address: </strong>
                <span className="text-yellow-500">{status.ipAddress}</span>
              </div>

              <div className="mb-2">
                <strong>Players: </strong>
                <span className="text-yellow-500">{status.players}</span>
              </div>

              <div className="mb-2">
                <strong>Current map: </strong>
                <span className="text-yellow-500">{status.currentMap}</span>
              </div>

              <div className="mb-2">
                <strong>Version: </strong>
                <span className="text-yellow-500">{status.version}</span>
              </div>
            </div>
          </div>

          {/* Server Metrics Section */}

          <div className="w-1/4 flex flex-col gap-4">
            <div className="flex flex-row gap-8">
              <h2 className="text-2xl font-bold mb-4">Server Metrics</h2>

              {loading && (
                <div>
                  <CircularProgress color="success" />
                </div>
              )}
            </div>

            {/* CPU Usage Graph */}
            <div className="bg-gray-800 p-4 rounded-lg">
              <h3 className="font-bold text-yellow-500">CPU Usage</h3>
              <div>
                {/* Add your CPU graph here */}
                <div className="h-24 bg-gray-700 flex items-center justify-center text-yellow-500">
                  {cpuUsage && cpuUsage}
                </div>
              </div>
            </div>

            {/* Memory Usage Graph */}
            <div className="bg-gray-800 p-4 rounded-lg">
              <h3 className="font-bold text-yellow-500">Memory Usage</h3>
              <div>
                {/* Add your memory graph here */}
                <div className="h-24 bg-gray-700 flex items-center justify-center text-yellow-500">
                  {memUsage && memUsage}
                </div>
              </div>
            </div>

            {/* Players Graph */}
            <div className="bg-gray-800 p-4 rounded-lg">
              <h3 className="font-bold text-yellow-500">Players</h3>
              <div>
                {/* Add your players graph here */}
                <div className="h-24 bg-gray-700 flex items-center justify-center text-yellow-500">
                  5/10
                </div>
              </div>
            </div>
          </div>
        </div>

        <div>
          <h2>Logs:</h2>
          <div style={styles.terminal}>
            {logs.map((log, index) => (
              <p key={index} style={styles.logText}>
                {log}
              </p>
            ))}
            {/* This element ensures the auto-scroll works */}
            <div ref={logsEndRef} />
          </div>
        </div>
      </div>
    </>
  );
};

const styles = {
  terminal: {
    backgroundColor: "#000",
    color: "#00ff00",
    fontFamily: "monospace",
    padding: "10px",
    borderRadius: "5px",
    height: "300px", // Restrict height
    overflowY: "scroll", // Make it scrollable
    whiteSpace: "pre-wrap", // Preserve white spaces and line breaks
    border: "1px solid #444", // Terminal-like border
  },
  logText: {
    margin: 0, // Remove margins for logs
  },
};

const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  overflowX: "auto",
  maxWidth: "80vh",
  maxHeight: "80vh",
  width: ["90%", "70%"],
};

export default ServerSettings;
