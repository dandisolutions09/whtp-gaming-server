import React, { useEffect, useState } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { IoMdPerson } from "react-icons/io";

import { FaCartShopping } from "react-icons/fa6";

import {
  AiOutlineMenu,
  AiOutlineHome,
  AiOutlineProject,
  AiOutlineMail,
  AiOutlineMenuFold,
} from "react-icons/ai";
import { GrProjects } from "react-icons/gr";
import { GoProjectRoadmap } from "react-icons/go";
import { FiPhone } from "react-icons/fi";

import { BsPerson } from "react-icons/bs";
import { FaAngleDown } from "react-icons/fa";

// import { styles } from "../styles";

const Navbar = () => {
  const [nav, setNav] = useState(false);
  const navigate = useNavigate();

  const [isDropdownOpen, setIsDropdownOpen] = useState(false); // State to toggle dropdown visibility
  const [selectedOption, setSelectedOption] = useState(""); // State for the selected option
  const [usr_obj, setUserObj] = useState();

  //   const navigate = useNavigate();

  function handleNav() {
    setNav(!nav);
    console.log("set changed");
  }
  const handleNavigate_home = () => {
    navigate("/");
  };

  const toggleDropdown = () => {
    setIsDropdownOpen((prev) => !prev); // Toggle dropdown visibility
  };

  const handleOptionClick = (option) => {
    setSelectedOption(option); // Update selected option

    const userID = localStorage.getItem("user-id");

    console.log("userD", userID);
    //setOpen(true);

    if (!userID) {
      console.log("user not registered!");

      setOpen(true);
    } else {
      navigate("/dashboard");

      setIsDropdownOpen(false); // Close dropdown after selection
    }
  };

  const handleLogout = (option) => {
    // setSelectedOption(option); // Update selected option

    // const userID = localStorage.getItem("user-id");

    localStorage.removeItem("user-id");
    navigate("/");

    // console.log("userD", userID);
    // //setOpen(true);

    // if (!userID) {
    //   console.log("user not registered!");

    //   setOpen(true);
    // } else {
    //   navigate("/dashboard");

    //   setIsDropdownOpen(false); // Close dropdown after selection
    // }
  };

  useEffect(() => {
    const storedUser = JSON.parse(localStorage.getItem("user"));

    console.log("stored user from navbar", storedUser);
    setUserObj(storedUser);

    // Cleanup when the component unmounts
    return () => {
      // eventSource.close();
    };
  }, []);

  return (
    <>
      <div className="absolute inline-block">
        <button
          onClick={toggleDropdown}
          className="px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600"
        >
          {isDropdownOpen ? "Hide Options" : "Show Options"}
        </button>
      </div>
      <div className="flex flex-col relative bg-gradient-to-r from-gray-950 to-gray-950 w-full md:w-500px">
        <AiOutlineMenuFold
          onClick={handleNav}
          className="absolute top-4 right-4 z-[99] md:hidden"
        />
        {nav ? (
          <div className="fixed w-full h-screen bg-white/90 flex flex-col justify-center items center z-20">
            <NavLink
              onClick={handleNav}
              to="/"
              className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
            >
              <AiOutlineHome size={20} color={"gray"} />
              <span className="pl-4 text-gray-500">Home</span>
            </NavLink>

            <NavLink
              onClick={handleNav}
              to="/projects"
              className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
            >
              <GoProjectRoadmap size={20} color={"gray"} />
              <span className="pl-4 text-gray-500">Projects</span>
            </NavLink>

            {/* <NavLink
            onClick={handleNav}
            to="/about"
            className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
          > */}
            <BsPerson size={20} color={"gray"} />
            <span className="pl-4 text-gray-500">About Us</span>
            {/* </NavLink> */}

            <NavLink
              onClick={handleNav}
              to="/members"
              className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
            >
              <AiOutlineProject size={20} color={"gray"} />
              <span className="pl-4 text-gray-500">Members</span>
            </NavLink>

            <NavLink
              onClick={handleNav}
              to="/contacts"
              className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
            >
              <AiOutlineMail size={20} color={"gray"} />
              <span className="pl-4 text-gray-500">Contact</span>
            </NavLink>
          </div>
        ) : (
          ""
        )}
        <div className="md:block hidden  relative z-10">
          <div className="absolute ml-[100px] md:ml-[59px] lg:ml-[100px] mt-[30px]">
            <div onClick={handleNavigate_home}>
              <h1 className="sm:text-xl lg:text-3xl md:text-3xl font-bol text-[#fff] cursor-pointer">
                We Have The Power Gaming Servers
              </h1>
            </div>
          </div>
          <div className="flex flex-row justify-end items-center shadow-md p-3 shadow-gray-400">
            <NavLink
              to="/"
              className="flex justify-center items-center m-2 p-3 cursor-pointer hover:scale-110 ease-in duration-300"
            >
              <AiOutlineHome size={20} color={"white"} />
              <span className="pl-4 text-[#fff] ">Home</span>
            </NavLink>
            <NavLink
              to="/service"
              className="flex justify-center items-center m-2 p-3 cursor-pointer hover:scale-110 ease-in duration-300"
            >
              <GoProjectRoadmap size={20} color={"white"} />
              <span className="pl-4 text-[#fff]">Our Products</span>
            </NavLink>
            {/* <NavLink
            to="/about"
            className="flex justify-center items-center m-2 p-3 cursor-pointer hover:scale-110 ease-in duration-300"
          > */}
            <BsPerson size={20} color={"white"} />
            <span className="pl-4 text-[#fff]">About</span>
            {/* </NavLink> */}

            <NavLink
              to="/faq"
              className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
            >
              {/* <AiOutlineMail size={20} color={"gray"} /> */}
              <span className="pl-2 text-[#fff]">FAQ</span>
            </NavLink>

            <div className="flex flex-row ml-4">
              <NavLink
                to="/about"
                className="flex justify-center items-center m-2 p cursor-pointer hover:scale-110 ease-in duration-300"
              >
                {/* <FaCartShopping size={20} color={"gray"} /> */}
              </NavLink>

              {/* <NavLink
              to="/contacts"
              className="flex justify-center items-center m-2 p cursor-pointer hover:scale-110 ease-in duration-300"
            > */}

              <div>
                {isDropdownOpen && (
                  <ul className="absolute top-[65px] right-[2px] w-48 h-40 mt-2 border border-gray-300 rounded shadow-lg bg-black overflow-y-auto z-10">
                    {/* Dropdown options */}
                    {/* <li
                      className="px-4 py-2 hover:bg-gray-200 cursor-pointer"
                      onClick={() => handleOptionClick("Option 1")}
                    >
                      Sign in
                    </li> */}

                    <li
                      className="px-4 py-2 hover:bg-gray-200 cursor-pointer"
                      onClick={() => handleOptionClick("Option 5")}
                    >
                      {usr_obj && usr_obj.email}
                    </li>
                    <li
                      className="px-4 py-2 hover:bg-gray-200 cursor-pointer"
                      onClick={() => handleOptionClick("Option 6")}
                    >
                      Sign in
                    </li>

                    <li
                      className="px-4 py-2 hover:bg-gray-200 cursor-pointer"
                      onClick={() => handleLogout("Option 6")}
                    >
                      Log out
                    </li>
                  </ul>
                )}

                {/* {selectedOption && (
                  <p className="mt-2 text-gray-700">
                    You selected: {selectedOption}
                  </p>
                )} */}
              </div>

              <div className=" flex flex-row gap-2" onClick={toggleDropdown}>
                <IoMdPerson size={20} color={"white"} />

                {usr_obj && usr_obj.first_name}

                <FaAngleDown />
              </div>

              {/* </NavLink> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
