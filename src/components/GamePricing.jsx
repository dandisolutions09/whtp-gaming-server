import React, { useState } from "react";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

import rust from "../assets/rust.jpg";
import { useNavigate } from "react-router-dom";
import Navbar from "./Navbar";

import endpoints from "../endpoints";
import GeneralSuccess from "./GeneralSuccess";
import GeneralError from "../Alerts/GeneralError";

//const AdminAddedSuccess = ({ open, onClose, autoHideDuration, name, mssg }) => {

// const Success = ({ open, onClose, autoHideDuration }) => {
const GamePricing = ({ open, onClose, autoHideDuration, msg }) => {
  const [generalError, setGeneralError] = useState();
  const [errorMsg, setErrorMsg] = useState();

  const [generalSuccess, setGeneralSuccess] = useState();
  const [successMsg, setSuccessMsg] = useState();

  const navigate = useNavigate();

  const updateUserPackage = async (data) => {
    var usr_id = localStorage.getItem("user_id");
    //var json_token = JSON.parse(storedToken);
    // console.log("Updating id", selectedRowData._id);

    const user = JSON.parse(localStorage.getItem("user"));

    const user_id = user._id;

    //console.log("TIER", tier);

    // const packageObject = {
    //   slots: 10,
    //   price: 1.69,
    //   sub_duration: 3,
    //   tier: "basic",

    //   // profile_pic: data.picture,
    // };

    const usrPkg = {
      slots: data.slots,
      price: data.price,
      tier: data.tier,
      // sub_duration: data.sub_duration,

      //game_name: data.game_name,
      // price: data.price,
      // slots: game_slots,
      // tier: tier,
    };

    console.log("main user package", usrPkg);

    try {
      // setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);

      const response = await fetch(
        `${endpoints.editUserPackage}?partner_id=${usr_id}&game_name=Minecraft`,

        //update-user?partner_id=

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            // Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(usrPkg),
        }
      );
      if (response.ok) {
        console.log("Records updated successfully");
        // To print the response body as JSON
        const responseData = await response.json();
        console.log("Response Data:", responseData);
        //setOpenProviderUpdateSuccess(true);
      } else {
        console.error("Failed to update record information");
        //setOpenProviderUpdateFailed(true);
      }
    } catch (error) {
      //setOpenProviderUpdateFailed(true);
      console.log("ERROR", error);
      setErrorMsg("something went wrong!");
      setGeneralError(true);

      console.error("Error sending data to the backend:", error);
    } finally {
      setSuccessMsg("successfully updated");
      setGeneralSuccess(true);

      setTimeout(() => {
        navigate("/checkout");
      }, 2000);
      // navigate("/checkout");

      //setSuccessMsg()
      // setLoading(false);
      // if (response.status === 200) {
      //   console.log("Records updated successfully");
      // } else {
      //   setOpenAddAdminFailed(true);
      //   setOpenAddCompany(true);
      //   console.error("Failed to update record information");
      // }
      // Set loading to false after the request is complete (success or error)
    }
    // setTimeout(() => {
    //   setOpenAddCompany(false);
    //   reloadPage();
    // }, 2000);
  };

  const handleChoosePackageButton = () => {
    // Handle "RENT RUST" button click
    console.log("Rent Rust button clicked");

    const packageObject = {
      slots: 13,
      price: 20.00,
      sub_duration: 3,
      tier: "basic",

      // profile_pic: data.picture,
    };

    updateUserPackage(packageObject);

    //setOpen(true);

    localStorage.setItem("user-package", JSON.stringify(packageObject));

    //const storedUser = JSON.parse(localStorage.getItem("user"));

    //navigate("/checkout");

    // Add your custom logic here
  };

  // useEffect(() => {
  //   const interval = setInterval(goToNext, 6000); // Slide every 1 second

  //   return () => clearInterval(interval); // Cleanup function
  // }, []);

  return (
    <>
      <Navbar />
      {/* Top Center Text */}

      <GeneralSuccess
        open={generalSuccess}
        autoHideDuration={6000}
        //name={"Success"}
        msg={successMsg}
        onClose={() => setGeneralSuccess(false)}
      />

      <GeneralError
        open={generalError}
        autoHideDuration={6000}
        Errmsg={errorMsg}
        onClose={() => setGeneralError(false)}
      />
      <div className="bg-gradient-to-r from-gray-950 to-gray-950">
        <div className="flex justify-center items-center h-screen relative mx-20 ">
          <div className="flex flex-col gap-10 ">
            <div className="text-center text-2xl">Available Packages</div>
            <div className="flex space-x-8">
              {/* Box 1 */}
              <div className="border border-gray-400 w-[320px] h-[320px] flex flex-col justify-between relative p-4">
                <div className="flex flex-col items-center justify-center h-full">
                  <h2 className="text-5xl text-center">$20.00</h2>
                  <h2 className="text-lg text-center">Basic</h2>
                </div>
                <div className="absolute top-0 right-0 text-2xl m-2">
                  5 Games
                </div>
                <div className="absolute top-0 left-0 text-xs m-2">4 Slots</div>
                <button
                  className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                  onClick={() => handleChoosePackageButton()}
                >
                  Choose Package
                </button>
              </div>

              {/* Box 2 */}
              <div className="border border-gray-400 w-[320px] h-[320px] flex flex-col justify-between relative p-4">
                <div className="flex flex-col items-center justify-center h-full">
                  <h2 className="text-5xl text-center">$21.69</h2>
                  <h2 className="text-lg text-center">Medium</h2>
                </div>
                <div className="absolute top-0 right-0 text-2xl m-2">
                  5 Games
                </div>
                <div className="absolute top-0 left-0 text-xs m-2">4 Slots</div>
                <button
                  className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                  onClick={handleChoosePackageButton}
                >
                  Choose Package
                </button>
              </div>

              {/* Box 3 */}
              <div className="border border-gray-400 w-[320px] h-[320px] flex flex-col justify-between relative p-4">
                <div className="flex flex-col items-center justify-center h-full">
                  <h2 className="text-5xl text-center">$26.59</h2>
                  <h2 className="text-lg text-center">Pro</h2>
                </div>
                <div className="absolute top-0 right-0 text-2xl m-2">
                  5 Games
                </div>
                <div className="absolute top-0 left-0 text-xs m-2">
                  10 Slots
                </div>
                <button
                  className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                  onClick={handleChoosePackageButton}
                >
                  Choose Package
                </button>
              </div>

              {/* Box 4 */}
              {/* <div className="border border-gray-400 w-[320px] h-[320px] flex flex-col justify-between relative p-4">
                <div className="flex flex-col items-center justify-center h-full">
                  <h2 className="text-lg text-center">
                    Create Your Own Configuration
                  </h2>
                  <h2 className="text-lg text-center">30 days Subscription</h2>
                </div>
                <div className="absolute top-0 right-0 text-2xl m-2">
                  5 Games
                </div>
                <div className="absolute top-0 left-0 text-xs m-2">
                  Customize
                </div>
                <button
                  className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                  onClick={handleChoosePackageButton}
                >
                  Choose Package
                </button>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default GamePricing;
