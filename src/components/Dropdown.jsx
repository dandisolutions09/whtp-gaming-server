import React, { useState } from 'react';

const Dropdown = () => {
  const [selectedOption, setSelectedOption] = useState(''); // State to keep track of the selected option

  const handleChange = (event) => {
    setSelectedOption(event.target.value); // Update state when user selects an option
  };

  return (
    <div>
      <label htmlFor="dropdown">Choose an option:</label>
      <select id="dropdown" value={selectedOption} onChange={handleChange}>
        <option value="">--Select--</option>
        <option value="option1">Option 1</option>
        <option value="option2">Option 2</option>
        <option value="option3">Option 3</option>
      </select>

      {selectedOption && <p>You selected: {selectedOption}</p>} {/* Display selected option */}
    </div>
  );
};

export default Dropdown;
