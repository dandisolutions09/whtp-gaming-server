import React, { useEffect, useState } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { FaGamepad } from "react-icons/fa";
// import logo from "../assets/rust.png";
import {
  AiOutlineMenu,
  AiOutlineHome,
  AiOutlineProject,
  AiOutlineMail,
} from "react-icons/ai";
import { IoIosArrowDown, IoIosArrowUp } from "react-icons/io";
import { FaBarcode } from "react-icons/fa";
import { BsGearFill } from "react-icons/bs";
import { GiSpanner } from "react-icons/gi";
import { MdWork } from "react-icons/md";
import { RiAccountPinCircleFill } from "react-icons/ri";
import { HiServer } from "react-icons/hi";
import { ImStatsDots } from "react-icons/im";
import { MdDashboard } from "react-icons/md";
import { GrProjects } from "react-icons/gr";
import { GoProjectRoadmap } from "react-icons/go";
import { MdGames } from "react-icons/md";
import { BsPerson } from "react-icons/bs";
import { IoGameControllerOutline } from "react-icons/io5";
import { FaCube } from "react-icons/fa";
import { IoIosLogIn } from "react-icons/io";
import { GiNotebook } from "react-icons/gi";
import { RiAccountCircleLine } from "react-icons/ri";
import { IoMdClose } from "react-icons/io";
import { PiDotFill } from "react-icons/pi";

import { styles } from "../styles";
import endpoints from "../endpoints";

const DashboardNav = () => {
  const [nav, setNav] = useState(false);
  const [loading, setLoading] = React.useState(false);
  const [bar, setBar] = useState(false);
  const [account, setAccount] = useState(false);
  const [toggle, setToggole] = useState(false);

  const navigate = useNavigate();
  const handleNav_home = () => {
    handleNav();
    navigate("/dashboard");
  };
  const handleNav_statistics = () => {
    handleNav();
    navigate("/statistics");
  };
  const handleNav = () => {
    setNav(!nav);
    console.log("nav changed");
    handleBar(); // Call handleBar after setting nav
  };

  const handleBar = () => {
    setBar(!bar); // Set bar based on the updated nav state
    console.log("bar changed");
  };
  const handleToggle = () => {
    setToggole(!toggle);
    console.log("nav changed");
    // Call handleBar after setting nav
  };

  // const handle_logout = () => {
  //   setLoading(true);
  //   setTimeout(() => {
  //     navigate("/");
  //   }, 1000);
  // };

  const handle_logout = async () => {
    console.log("logout function called");
    setLoading(true);

    try {
      setLoading(true); // Set loading to true when making the request
      setOpenAddAdminSuccess(true);
      const headers = {
        Authorization: `Bearer ${json_token.token}`,
        "Content-Type": "application/json", // Assuming your endpoint expects JSON data
      };
      const response = await axios.post(endpoints.logout);
      fmt.Println("response", response);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
      // handleClose_AddCompany();
      setTimeout(() => {
        navigate("/");
      }, 1000);
    }
  };

  return (
    <div className="select-none">
      {account && <div>jesse</div>}
      {bar ? (
        <div className="hidden justify-between sm:hidden  md:hiddden top-1  z-[50] bg-[#2d3544]  h-14  lg:hidden select-none">
          <AiOutlineMenu
            color="#804dee"
            onClick={handleNav}
            className="m-4 z-[50] md:hiddden text-[#804dee] select-none"
          />
          <div className="flex flex-row gap-2 cursor-pointer tracking-widest p-2">
            <IoGameControllerOutline size={30} color="#EE9B00" />
            <h1 className="font-semibold  text-[#EE9B00] mt-1 text-xl">
              FineFoods
            </h1>
          </div>
          <RiAccountCircleLine size={25} color="#EE9B00" className="m-3" />
        </div>
      ) : (
        <div className="flex justify-between sm:hidden md:hiddden top-1  z-[1000] bg-[#000]  h-14  lg:hidden">
          <AiOutlineMenu
            color="#fff"
            size={24}
            onClick={handleNav}
            className="m-4 z-[50] md:hiddden text-[#fff] select-none"
          />
          <div className="flex flex-row gap-2 cursor-pointer tracking-widest p-2 select-none">
            {/* <img src={logo} alt="logo" /> */}
            <h1 className="font-semibold  text-[#fff] mt-1 text-xl">
              FineFoods
            </h1>
          </div>
          <RiAccountCircleLine size={25} color="#fff" className="m-3" />
        </div>
      )}
      {nav ? (
        <div className="fixed md:hidden w-full h-screen bg-[#215233] flex flex-col  z-[999]">
          <div onClick={handleNav} className="m-5 cursor-pointer">
            <IoMdClose size={24} />
          </div>

          <div className="flex flex-col ml-6 ">
            <h1 className="m-3 text-md font-semibold">PRODUCTS</h1>
            <NavLink
              onClick={handleNav}
              to="/dashboard"
              className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1 "
            >
              <MdDashboard size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">My Services</p>
            </NavLink>

            <NavLink
              to="/statistics"
              className="m-4  btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
            >
              <ImStatsDots size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400 ">Map View</p>
            </NavLink>

            <h1 className="m-3 text-md font-semibold">MANAGEMENT</h1>

            <div
              onClick={handleToggle}
              className="m-4 cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1 btn"
            >
              <BsPerson size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">Admin</p>
              {toggle ? (
                <IoIosArrowUp size={20} className="text-gray-400" />
              ) : (
                <IoIosArrowDown />
              )}
            </div>

            {toggle && (
              <div className="ml-6 flex flex-col gap-1  py-2 justify-center">
                <NavLink
                  to="/admin"
                  className="text-sm cursor-pointer p-1 btn flex flex-row"
                >
                  <PiDotFill size={24} /> <p>Admins</p>
                </NavLink>
                <NavLink
                  to="/admingroups"
                  className="text-sm cursor-pointer btn p-1 flex flex-row"
                >
                  <PiDotFill size={24} /> <p>Admin Groups</p>
                </NavLink>
              </div>
            )}
            <NavLink
              to="/users"
              className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
            >
              <RiAccountPinCircleFill size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">Customer</p>
            </NavLink>

            <NavLink
              to="/providers"
              className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
            >
              <HiServer size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">Drivers Distractions</p>
            </NavLink>
            {/* <NavLink
              to="/manageservices"
              className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
            >
              <GiSpanner size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">Services</p>
            </NavLink> */}
            <h1 className="m-3 text-md font-semibold">BUSINESS</h1>

            <NavLink
              to="/manualbooking"
              className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
            >
              <FaCube size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">Orders</p>
            </NavLink>

            <NavLink
              to="/manualbooking"
              className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
            >
              <FaCube size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">Financials</p>
            </NavLink>

            {/* <NavLink
              to="/company"
              className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
            >
              <MdWork size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">Jobs</p>
            </NavLink> */}
            {/* <NavLink
              to="/company"
              className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
            >
              <AiOutlineProject size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">Company</p>
            </NavLink>

            <NavLink
              to="/company"
              className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
            >
              <FaCube size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">Scheduled Bookings</p>
            </NavLink> */}
            {/* <NavLink
              to="/company"
              className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
            >
              <FaBarcode size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">PromoCode</p>
            </NavLink> */}
            <NavLink
              to="/logs"
              className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
            >
              <GrProjects size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">Logs</p>
            </NavLink>
            <NavLink
              to="/company"
              className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
            >
              <BsGearFill size={20} className="text-gray-400" />
              <p className="text-sm text-gray-400">Settings</p>
            </NavLink>
            <div className=" m-4  flex space-x-10">
              <span className="cursor-pointer" onClick={handle_logout}>
                <IoIosLogIn size={25} />
                <p className="text-sm">Logout</p>
              </span>
              <span className="cursor-pointer">
                <GiNotebook size={25} />
                <p className="text-sm">Register</p>
              </span>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
      <div className="fixed md:block  sm:block hidden  z-10 bg-[#000] h-screen w-[300px] overflow-auto">
        <div className="z-50 m-4 py-2 px-2">
          <div className="flex flex-row gap-3  tracking-widest border-b py-4 w-full">
            <img
              // src={logo}
              alt="logo"
              className="cursor-pointer"
              onClick={handleNav_home}
            />
          </div>
        </div>
        <div className="flex flex-col ml-6 ">
          <h1 className="m-3 text-md font-semibold">PRODUCTS</h1>
          <NavLink
            to="/dashboard"
            className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1 "
          >
            <MdDashboard size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">My Services</p>
          </NavLink>

          <NavLink
            to="/statistics"
            className="m-4  btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
          >
            <ImStatsDots size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400 ">Map View</p>
          </NavLink>

          <h1 className="m-3 text-md font-semibold">MANAGEMENT</h1>

          <div
            onClick={handleToggle}
            className="m-4 cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1  bg-black"
          >
            <BsPerson size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">Admins</p>
            {toggle ? (
              <IoIosArrowUp size={20} className="text-gray-400" />
            ) : (
              <IoIosArrowDown />
            )}
          </div>

          {toggle && (
            <div className="ml-6 flex flex-col gap-1  py-2 justify-center">
              <NavLink
                to="/admin"
                className="text-sm cursor-pointer p-1 btn flex flex-row"
              >
                <PiDotFill size={24} /> <p>Admin</p>
              </NavLink>
              <NavLink
                to="/admingroups"
                className="text-sm cursor-pointer btn p-1 flex flex-row"
              >
                <PiDotFill size={24} /> <p>Admin Groups</p>
              </NavLink>
            </div>
          )}
          <NavLink
            to="/users"
            className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
          >
            <RiAccountPinCircleFill size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">Customers</p>
          </NavLink>

          <NavLink
            to="/providers"
            className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
          >
            <HiServer size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">Driver Distractions</p>
          </NavLink>
          {/* <NavLink
            to="/manageservices"
            className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
          >
            <GiSpanner size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">Services</p>
          </NavLink> */}
          {/* <h1 className="m-3 text-md font-semibold">BUSINESS</h1>

          <NavLink
            to="/manualbooking"
            className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
          >
            <FaCube size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">Orders</p>
          </NavLink>

          <NavLink
            to="/financials"
            className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
          >
            <FaCube size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">Financials</p>
          </NavLink> */}
          {/* <NavLink
            to="/company"
            className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
          >
            <MdWork size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">Jobs</p>
          </NavLink> */}
          {/* <NavLink
            to="/company"
            className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
          >
            <AiOutlineProject size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">Company</p>
          </NavLink> */}
          {/* <NavLink
            to="/company"
            className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
          >
            <FaCube size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">Scheduled Bookings</p>
          </NavLink> */}
          {/* <NavLink
            to="/motions"
            className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
          >
            <FaBarcode size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">PromoCode</p>
          </NavLink> */}
          <NavLink
            to="/logs"
            className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
          >
            <GrProjects size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">Logs</p>
          </NavLink>
          <NavLink
            to="/company"
            className="m-4 btn cursor-pointer hover:scale-110 ease-in duration-300 flex flex-row gap-1"
          >
            <BsGearFill size={20} className="text-gray-400" />
            <p className="text-sm text-gray-400">Settings</p>
          </NavLink>
          <div className=" m-4  flex space-x-10">
            <span className="cursor-pointer" onClick={handle_logout}>
              <IoIosLogIn size={25} />
              <p className="text-sm">Logout</p>
            </span>
            <span className="cursor-pointer">
              <GiNotebook size={25} />
              <p className="text-sm">Register</p>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DashboardNav;
