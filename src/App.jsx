import { HashRouter, Route, Routes, useLocation } from "react-router-dom";
import HomePage from "./pages/HomePage";
import Navbar from "./components/Navbar";
import Gameservers from "./pages/Gameservers";
import Minecraftservers from "./pages/Minecraftservers";
import ConfigPage from "./pages/ConfigPage";
import GamePricing from "./components/GamePricing";
import Checkout from "./pages/Checkout";
import { ThemeProvider, createTheme } from "@mui/material";
import Dashboard from "./pages/Dashboard";
import ServerSettings from "./components/ServerSettings";

function App() {
  // const location = useLocation(); // Get the current route location
  const theme = createTheme({
    typography: {
      fontFamily: ["Play"],
      backdropFilter: "none",
    },
  });
  return (
    <ThemeProvider theme={theme}>
      <HashRouter>
       
        <Routes>
          <Route index element={<HomePage />} />
          <Route path="/" element={<HomePage />} />
          <Route path="/gameserver" element={<Gameservers />} />
          <Route path="/minecraftserver" element={<Minecraftservers />} />
          <Route path="/configpage" element={<ConfigPage />} />
          <Route path="/game-pricing" element={<GamePricing />} />
          <Route path="/checkout" element={<Checkout />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/server-settings" element={<ServerSettings />} />
        </Routes>

        {/* Conditionally render Footer based on the current route */}

        {/* <Footer /> */}
      </HashRouter>
    </ThemeProvider>
  );
}

export default App;
