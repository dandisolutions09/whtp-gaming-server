//const BASE_URL = "https://finelooks-backend-service.onrender.com";
//const BASE_URL = "http://192.168.100.71:8081";
//const BASE_URL = "http://localhost:8081";

// const BASE_URL = "http://50.4.71.78:8081";

const BASE_URL = "https://api.w3havethepower.com";

//sss

//const LOCAL_BASE_URL = "http://localhost:8080"; // Your base URL
const GOOGLE_API =
  "https://maps.googleapis.com/maps/api/place/autocomplete/json";

const GOOGLE =
  "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyDolZ9dXT3cqWqqHt6XTyojZlzt1EmtZoo&input=Ngumba&types=geocode&components=country:KE";

//const BASE_URL = "https://8301-41-80-117-153.ngrok-free.app";
//const BASE_URL = "http://localhost:8080";

//https://8301-41-80-117-153.ngrok-free.app

const endpoints = {
  // login: `${BASE_URL}/admin-login`,
  // getAdmins: `${BASE_URL}/get-admins`,
  // addAdmin: `${BASE_URL}/add-admin`,
  // getAdminById: `${BASE_URL}/get-admin`,
  // resetAdminPassword: `${BASE_URL}/admin-reset-password`,
  // editAdminInfo: `${BASE_URL}/update-admin`,

  //partner endpoints
  //getPartnerById: `${LOCAL_BASE_URL}/get-partner-by-id`,
  registerVisitor: `${BASE_URL}/register-visitor`,
  visitorLogin: `${BASE_URL}/user-login`,

  //LOGOUT
  logout: `${BASE_URL}/logout`,

  //GET STATS
  getResourcesUsage: `${BASE_URL}/get-resources-usage`,

  //filterbooking status
  filterBookingStatus: `${BASE_URL}/filter-bookings`,

  starServer: `${BASE_URL}/start-server`,

  editUserInfo: `${BASE_URL}/update-user`,

  editUserPackage: `${BASE_URL}/update-user-package`,

  stopServer: `${BASE_URL}/stop-server`,

  getServerStats: `${BASE_URL}/sse/docker-stats`,

  getContainerStats: `${BASE_URL}/sse/containers-stats`,

  followLogs: `${BASE_URL}/logs`,

  getUSer: `${BASE_URL}/get-user`,

  getContainerResourceUsage: `${BASE_URL}/get-container-resource-usage`,

  postRustServerConfig: `${BASE_URL}/modify-rust-server-config`,
};

export default endpoints;
