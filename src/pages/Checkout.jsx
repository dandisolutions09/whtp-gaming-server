import React, { useEffect, useState } from "react";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

import rust from "../assets/rust.jpg";

import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { IoMdCloseCircle } from "react-icons/io";
import { IoLogoPaypal } from "react-icons/io5";
import { RiErrorWarningLine, RiVisaFill } from "react-icons/ri";

import {
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  StepButton,
} from "@mui/material";
import { RiAccountPinCircleFill } from "react-icons/ri";
import { HiServer } from "react-icons/hi";
import { FaCcMastercard } from "react-icons/fa";
import { SiAmericanexpress } from "react-icons/si";
import { AiOutlineCheck } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import Navbar from "../components/Navbar";
import GeneralError from "../Alerts/GeneralError";
import endpoints from "../endpoints";
import axios from "axios";

const steps = ["Games Picked", "Tier Selected", "Buy"];

export default function Checkout() {
  const navigate = useNavigate();
  const [activeStep, setActiveStep] = React.useState(2);
  const [skipped, setSkipped] = React.useState(new Set());
  //const [completed, setCompleted] = React.useState({});
  const [completed, setCompleted] = useState({ 0: true, 1: true }); // Step 1 and Step 2 are marked complete
  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState(null);

  const [selectedCountry, setSelectedCountry] = useState("");

  const [game_price, setPricing] = useState("");

  const [game_name, setGameName] = useState("");

  const [game_slots, setSlots] = useState("");

  const [tier, setGameTier] = useState("");

  const [game_pic, setGamePic] = useState("");

  const [generalError, setGeneralError] = useState();

  const [errorMsg, setErrorMsg] = useState();
  const [successMsg, setSuccessMsg] = useState();

  const [data, setData] = useState();

  // const

  //setSlots
  //setGamePic

  //setPricing

  const handleSelect = (paymentType) => {
    setSelectedPaymentMethod(paymentType);

    localStorage.setItem("payment_method", paymentType);
  };

  const isStepOptional = (step) => {
    return step === 1;
  };

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleComplete = () => {
    const newCompleted = completed;
    newCompleted[activeStep] = true;
    console.log("active step", activeStep);

    console.log("new completed", newCompleted[activeStep]);
    setCompleted(newCompleted);
    handleNext();
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const updateUserData = async (data) => {
    var usr_id = localStorage.getItem("user_id");
    //var json_token = JSON.parse(storedToken);
    // console.log("Updating id", selectedRowData._id);

    const user = JSON.parse(localStorage.getItem("user"));

    const user_id = user._id;

    // const partnerRegistrationObject = {
    //   email: data.email.trim(),
    //   //password: data.password.trim(),
    //   first_name: data.first_name,
    //   last_name: data.last_name,
    //   phone_number: data.phone_number,
    //   main_businessName: data.main_businessName,
    //   bio: data.bio,
    //   profile_pic: pp.image_url,
    //   logo: bp,
    // };

    // const game_specs = {
    //   slots: userPackage.slots,
    // };

    console.log("TIER", tier);

    const usr = {
      payment_method: data.payment_method,
      country: data.country,
      // game_name: data.game_name,
      // price: data.price,
      // slots: game_slots,
      // tier: tier,
    };

    console.log("main object", usr);

    try {
      // setLoading(true); // Set loading to true when making the request
      //setOpenAddAdminSuccess(true);

      const response = await fetch(
        `${endpoints.editUserInfo}?partner_id=${usr_id}&game_name=Minecraft`,

        //update-user?partner_id=

        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            // Authorization: `Bearer ${json_token.token}`,
          },
          body: JSON.stringify(usr),
        }
      );
      if (response.ok) {
        console.log("Records updated successfully");
        // To print the response body as JSON
        const responseData = await response.json();
        console.log("Response Data:", responseData);
        //setOpenProviderUpdateSuccess(true);
      } else {
        console.error("Failed to update record information");
        //setOpenProviderUpdateFailed(true);
      }
    } catch (error) {
      //setOpenProviderUpdateFailed(true);
      console.log("ERROR", error);

      console.error("Error sending data to the backend:", error);
    } finally {
      //setSuccessMsg()
      // setLoading(false);
      // if (response.status === 200) {
      //   console.log("Records updated successfully");
      // } else {
      //   setOpenAddAdminFailed(true);
      //   setOpenAddCompany(true);
      //   console.error("Failed to update record information");
      // }
      // Set loading to false after the request is complete (success or error)
    }
    // setTimeout(() => {
    //   setOpenAddCompany(false);
    //   reloadPage();
    // }, 2000);
  };

  const handleChoosePackageButton = (price) => {
    // Handle "RENT RUST" button click
    console.log("Rent Rust button clicked", price);

    //localStorage.setItem("country", selectedValue);

    const userCountry = localStorage.getItem("country");

    const userPaymentMethod = localStorage.getItem("payment_method");

    if (selectedCountry == "" || userPaymentMethod == "") {
      console.log("ERROR ON COUNTRY SELECTION");
      setErrorMsg("Neither Country Nor Payment Method is selected");
      setGeneralError(true);
      return;
    }

    console.log(selectedCountry);

    console.log(userPaymentMethod);

    const userPackage = {
      payment_method: userPaymentMethod,
      country: selectedCountry,
     // game_name: game_name,
     // price: game_price,
      //tier: tier,
    };

    console.log("userPackage", userPackage);

    updateUserData(userPackage);

    //payment_method

    //setOpen(true);

    navigate("/dashboard");

    // Add your custom logic here
  };

  const handleChange = (event) => {
    const selectedValue = event.target.value;
    setSelectedCountry(selectedValue);
    console.log("Selected Country:", selectedValue);

    localStorage.setItem("country", selectedValue);

    // const storedUser = JSON.parse(localStorage.getItem("user"));
  };

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("You can't skip a step that isn't optional.");
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values());
      newSkipped.add(activeStep);
      return newSkipped;
    });
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const handleClose_EditProvider = () => {
    setOpenEdit(false);
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const totalSteps = () => {
    return steps.length;
  };

  const completedSteps = () => {
    return Object.keys(completed).length;
  };

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps();
  };

  const menuItems = [
    { value: "Kenya", label: "Kenya" },
    { value: "United States", label: "United States" },
    // { value: "INACTIVE", label: "INACTIVE" },
    // { value: "FEATURED", label: "FEATURED" },
  ];

  const fetchData = async () => {
    var usr_id = localStorage.getItem("user_id");

    try {
      const response = await axios.get(
        `${endpoints.getUSer}?user_id=${usr_id}`
      ); // Replace with your API URL
      console.log("data", response.data.user)
      setData(response.data.user); // Store the data in state
    } catch (err) {
      setErrorMsg(err.message);
      setGeneralError(true);
    } finally {
      //setSuccessMsg("Success")
      //setLoading(false); // Update loading state after the request
    }
  };

  useEffect(() => {
    console.log("use effect called");
    const userPackage = JSON.parse(localStorage.getItem("user-package"));

    console.log("user package", userPackage);

    const gameObject = JSON.parse(localStorage.getItem("game-object"));

    const usergame = localStorage.getItem("user-game");

    console.log("user package tier", userPackage.tier);

    fetchData();

    // setPricing(userPackage.price);

    // setSlots(userPackage.slots);

    // setGameName(gameObject.game_name);
    // setGamePic(gameObject.game_pic_url);
    // setGameTier(userPackage.tier);

    // const interval = setInterval(goToNext, 6000); // Slide every 1 second

    // return () => clearInterval(interval); // Cleanup function

    // const newCompleted = completed;
    // newCompleted[2] = true;
    // setCompleted(newCompleted);
    //handleNext();
  }, []);

  return (
    <>
      <GeneralError
        open={generalError}
        autoHideDuration={6000}
        Errmsg={errorMsg}
        onClose={() => setGeneralError(false)}
      />
      {/* <Navbar /> */}
      <Box sx={style_box} className="select-none">
        <div className="flex flex-row justify-between m-2 py-2">
          <Typography
            sx={{ color: "#fff", fontSize: [18, 24], fontWeight: "bold" }}
          >
            Book Services
          </Typography>
          {/* <IoMdCloseCircle
            size={18}
            onClick={handleClose_EditProvider}
            cursor={"pointer"}
            color="#000"
          /> */}
        </div>
        <Stepper nonLinear activeStep={activeStep}>
          {steps.map((label, index) => (
            <Step key={label} completed={completed[index]}>
              <StepButton onClick={handleStep(index)}>
                <StepLabel sx={{ color: "white" }}>{label}</StepLabel>
              </StepButton>
            </Step>
          ))}
        </Stepper>
        <div>
          {allStepsCompleted() ? (
            <React.Fragment>
              <Typography sx={{ mt: 2, mb: 1 }}>
                All steps completed - you&apos;re finished
              </Typography>
              <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
                <Box sx={{ flex: "1 1 auto" }} />
                <Button onClick={handleReset}>Reset</Button>
              </Box>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Typography
                sx={{
                  mt: 2,
                  mb: 1,
                  py: 1,
                  color: "#fff",
                }}
              >
                {/* Step {activeStep + 1} */}
                {activeStep === 0 ? (
                  // <p>First form</p>
                  <>
                    <div className=" sm:mt-8 mt-1 ">
                      {/* <Search sx={{ border: 1, borderColor: "#DAD8C9" }}>
                      <SearchIconWrapper>
                        <SearchIcon sx={{ color: "#616569" }} />
                      </SearchIconWrapper>
                      <StyledInputBase
                        value={searchTerm_customers}
                        onChange={handleSearchChange_Customer}
                        placeholder="Search Customer…"
                        inputProps={{ "aria-label": "search" }}
                      />
                    </Search> */}
                    </div>

                    <div>
                      <div className="flex flex-row items-center">
                        <Typography
                          sx={{
                            fontWeight: "bold",
                            fontSize: [14, 18],
                            margin: 1,
                          }}
                        >
                          Customers
                        </Typography>
                        <RiAccountPinCircleFill
                          size={24}
                          className="text-gray-400"
                        />
                      </div>
                      {/* <ul
                      className="m-2 rounded shadow-lg py-2"
                      style={{
                        overflowY: "scroll",
                        maxHeight: "40vh",
                      }}
                    >
                      {activeCustomers &&
                        activeCustomers.length > 0 &&
                        activeCustomers.map((item, index) => (
                          <LightTooltip title="Select Customer">
                            <li
                              key={index}
                              // onClick={() => handleAddCustomer(item)}
                              onClick={() => handleConfirm_pick(item)}
                              className="py-3 border-b cursor-pointer border-gray-300 hover:bg-slate-300 duration-200 ease-linear"
                            >
                              <div className="flex flex-row justify-between mx-2">
                                <Typography
                                  sx={{
                                    fontSize: [12, 13],
                                  }}
                                >
                                  {item.first_name}
                                </Typography>
                                <Typography
                                  sx={{
                                    fontSize: [12, 13],
                                  }}
                                >
                                  {item.last_name}
                                </Typography>
                                <Typography
                                  sx={{
                                    fontSize: [12, 13],
                                  }}
                                >
                                  {item.phone_number}
                                </Typography>
                                <Typography
                                  sx={{
                                    fontSize: [12, 13],
                                  }}
                                >
                                  {item.email}
                                </Typography>
                              </div>
                            </li>
                          </LightTooltip>
                        ))}
                    </ul> */}
                    </div>

                    {/* <form
                    onSubmit={formikPartnerAvailability.handleSubmit}
                    className="w-full flex flex-col"
                  >
                

                    <div className="flex flex-col gap-3 mt-3">
                      <div>
                        <Button
                          variant="contained"
                          sx={{ backgroundColor: "#242333" }}
                          size="small"
                          type="submit"
                        >
                          Save
                        </Button>
                      </div>
                    </div>
                  </form> */}
                  </>
                ) : activeStep === 1 ? (
                  <>
                    <div className=" sm:mt-8 mt-1 ">
                      {/* <Search sx={{ border: 1, borderColor: "#DAD8C9" }}>
                      <SearchIconWrapper>
                        <SearchIcon sx={{ color: "#616569" }} />
                      </SearchIconWrapper>
                      <StyledInputBase
                        value={searchTerm_providers}
                        onChange={handleSearchChange_Providers}
                        placeholder="Search Provider…"
                        inputProps={{ "aria-label": "search" }}
                      />
                    </Search> */}
                    </div>

                    <div>
                      <div className="flex flex-row items-center">
                        <Typography
                          sx={{
                            fontWeight: "bold",
                            fontSize: [14, 18],
                            margin: 1,
                          }}
                        >
                          Providers
                        </Typography>
                        {/* <HiServer size={24} className="text-gray-400" /> */}
                      </div>
                      {/* <ul
                      className="m-2 rounded shadow-lg py-2"
                      style={{
                        overflowY: "scroll",
                        maxHeight: "40vh",
                      }}
                    >
                      {activeProviders &&
                        activeProviders.length > 0 &&
                        activeProviders.map((item, index) => (
                          <LightTooltip title="Select service provider">
                            <li
                              key={index}
                              // onClick={() => handleAddCustomer(item)}
                              onClick={() => handleConfirm_pick_provider(item)}
                              className="py-3 border-b cursor-pointer border-gray-300 hover:bg-slate-300 duration-200 ease-linear"
                            >
                              <div className="flex flex-row justify-between mx-2">
                                <Typography
                                  sx={{
                                    fontSize: [12, 13],
                                  }}
                                >
                                  {item.first_name}
                                </Typography>
                                <Typography
                                  sx={{
                                    fontSize: [12, 13],
                                  }}
                                >
                                  {item.last_name}
                                </Typography>
                                <Typography
                                  sx={{
                                    fontSize: [12, 13],
                                  }}
                                >
                                  {item.phone_number}
                                </Typography>
                                <Typography
                                  sx={{
                                    fontSize: [12, 13],
                                  }}
                                >
                                  {item.email}
                                </Typography>
                              </div>
                            </li>
                          </LightTooltip>
                        ))}
                    </ul> */}
                    </div>

                    {/* <form
                    onSubmit={formikPartnerAvailability.handleSubmit}
                    className="w-full flex flex-col"
                  >
                

                    <div className="flex flex-col gap-3 mt-3">
                      <div>
                        <Button
                          variant="contained"
                          sx={{ backgroundColor: "#242333" }}
                          size="small"
                          type="submit"
                        >
                          Save
                        </Button>
                      </div>
                    </div>
                  </form> */}
                  </>
                ) : activeStep === 2 ? (
                  <>
                    {/* <div className="flex flex-col gap-3 mt-3">
                    <div>
                      <Button
                        variant="contained"
                        sx={{ backgroundColor: "#242333" }}
                        size="small"
                        //onClick={submitBookingDateTime}
                      >
                        Finish Time Selection
                      </Button>
                    </div>
                  </div> */}

                    <div className="flex flex-col gap-10 pt-2 ">
                      {/* <div className="text-center text-black text-2xl">
                      Available Packages
                    </div> */}
                      <div className="flex justify-center space-x-[100px]">
                        {/* Box 1 */}
                        <div className="border border-gray-400 w-[620px] h-[620px] flex flex-col justify-between relative p-4">
                          <div className="flex flex-col items-center  h-full mt-[50px] gap-4">
                            {/* <h2 className="text-xl text-center text-black">
                            Country*
                          </h2> */}
                            {/* <h2 className="text-lg text-black text-center">
                            3 days Subscription
                          </h2> */}

                            <div className="absolute top-5 left-5 text-black text-2xl m-2">
                              Choose Payment Method
                            </div>

                            <FormControl fullWidth sx={{ mt: 15, mb: 10 }}>
                              <InputLabel id="demo-simple-select-label">
                                Country
                              </InputLabel>
                              <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={selectedCountry}
                                label="Age"
                                onChange={handleChange}
                              >
                                {/* <MenuItem value={10}>Ten</MenuItem>
                              <MenuItem value={20}>Twenty</MenuItem>
                              <MenuItem value={30}>Thirty</MenuItem> */}

                                {menuItems.map((item, index) => (
                                  <MenuItem key={index} value={item.value}>
                                    {item.label}
                                  </MenuItem>
                                ))}
                              </Select>
                            </FormControl>

                            <div className="flex flex-row gap-4">
                              {/* PayPal */}
                              <div
                                className={`relative border ${
                                  selectedPaymentMethod === "paypal"
                                    ? "border-yellow-400 border-2"
                                    : "border-gray-400"
                                } p-8 rounded cursor-pointer`}
                                onClick={() => handleSelect("paypal")}
                              >
                                {selectedPaymentMethod === "paypal" && (
                                  <AiOutlineCheck className="absolute top-1 right-1 text-black" />
                                )}
                                <IoLogoPaypal size={58} color="#000" />
                              </div>

                              {/* Visa */}
                              <div
                                className={`relative border ${
                                  selectedPaymentMethod === "visa"
                                    ? "border-yellow-400 border-2"
                                    : "border-gray-400"
                                } p-8 rounded cursor-pointer`}
                                onClick={() => handleSelect("visa")}
                              >
                                {selectedPaymentMethod === "visa" && (
                                  <AiOutlineCheck className="absolute top-1 right-1 text-black" />
                                )}
                                <RiVisaFill size={58} color="#000" />
                              </div>

                              {/* Mastercard */}
                              <div
                                className={`relative border ${
                                  selectedPaymentMethod === "mastercard"
                                    ? "border-yellow-400 border-2"
                                    : "border-gray-400"
                                } p-8 rounded cursor-pointer`}
                                onClick={() => handleSelect("mastercard")}
                              >
                                {selectedPaymentMethod === "mastercard" && (
                                  <AiOutlineCheck className="absolute top-1 right-1 text-black" />
                                )}
                                <FaCcMastercard size={58} color="#000" />
                              </div>

                              {/* American Express */}
                              <div
                                className={`relative border ${
                                  selectedPaymentMethod === "amex"
                                    ? "border-yellow-400 border-2"
                                    : "border-gray-400"
                                } p-8 rounded cursor-pointer`}
                                onClick={() => handleSelect("amex")}
                              >
                                {selectedPaymentMethod === "amex" && (
                                  <AiOutlineCheck className="absolute top-1 right-1 text-black" />
                                )}
                                <SiAmericanexpress size={58} color="#000" />
                              </div>
                            </div>
                          </div>

                          {/* <div>
                          <div className=" text-black text-2xl m-2">
                            Choose Payment Method
                          </div>
                        </div> */}

                          {/* <div className="absolute top-0 left-0 text-xs m-2">
                          4 Slots
                        </div> */}
                          {/* <button
                          className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                          onClick={handleChoosePackageButton}
                        >
                          Choose Package
                        </button> */}
                        </div>

                        {/* Box 2 */}

                        <div className="border border-gray-400 w-[620px] h-[620px] flex flex-col relative p-4">
                          <div className="flex flex-col items-left justify-center h-full">
                            {/* <h2 className="text-5xl text-center text-black">$1.69</h2> */}
                            {/* <div>
      <h2 className="text-lg text-left text-black">Features</h2>
    </div> */}

                            <div className="absolute top-2 left-5 text-black text-2xl m-2">
                              Order Summary
                            </div>

                            <div className="absolute top-[50px] right-5 text-black text-2xl m-2">
                              ${data && data.price}
                            </div>

                            {/* Game Server Details */}
                            <div className="absolute top-[50px] left-0 flex items-top p-4">
                              {/* <img
                                src={game_pic}
                                alt="Rust Server Hosting"
                                className="w-[80px] h-[100px]"
                              /> */}
                              <div>
                                <div className="text-center text-black px-4 font-bold">

                                  
                                  {data && data.tier} (
                                  {data && data.slots} Slots)
                                </div>
                                {/* <div className="text-left text-black px-4">
                                  Rust Survival
                                </div> */}
                              </div>
                            </div>
                          </div>

                          {/* Subtotal and Discount */}
                          <div className="flex flex-col gap-2 mt-auto mb-4">
                            <div className="flex flex-row justify-between">
                              <h2 className="text-lg text-left text-black">
                                Subtotal
                              </h2>
                              <h2 className="text-lg text-left text-black">
                                ${data && data.price}
                              </h2>
                            </div>
                            <div className="flex flex-row justify-between">
                              <h2 className="text-lg text-left text-black">
                                Discount
                              </h2>
                              <h2 className="text-lg text-left text-black">
                                -$0.99
                              </h2>
                            </div>

                            <div className="border-t border-gray-200 my-4"></div>

                            <div className="flex flex-row justify-between">
                              <h2 className="text-lg text-left text-black">
                                Grand Total
                              </h2>
                              <h2 className="text-lg text-left text-black font-bold">
                                $
                                {data
                                  ? (parseFloat(data.price) - 0.99).toFixed(2)
                                  : ""}
                              </h2>
                            </div>
                          </div>
                          <div className="border-t border-black my-4"></div>

                          {/* <RiErrorWarningLine /> */}

                          <div className="flex flex-row gap-2">
                            <RiErrorWarningLine size={28} color="#000" />
                            <p className="text-slate-500  text-xs">
                              Total price of ${" "}
                              {game_price
                                ? (parseFloat(game_price) - 0.99).toFixed(2)
                                : ""}{" "}
                              will be charged monthly as subscription. You can
                              cancel your subscription anytime.
                            </p>
                          </div>

                          <button
                            className="bg-[#EE9B00] text-black text-xsm px-4 py-2 rounded mt-4"
                            onClick={() =>
                              handleChoosePackageButton(
                                game_price
                                  ? (parseFloat(game_price) - 0.99).toFixed(2)
                                  : ""
                              )
                            }
                          >
                            Order Package{" "}
                            <span className="font-bold">
                              $
                              {game_price
                                ? (parseFloat(game_price) - 0.99).toFixed(2)
                                : ""}
                            </span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </>
                ) : activeStep === 3 ? (
                  <>
                    Checkout
                    <div className="flex flex-col gap-3 mt-3">
                      <div>
                        <Button
                          variant="contained"
                          sx={{ backgroundColor: "#242333" }}
                          size="small"
                          //onClick={submitBookingDateTime}
                        >
                          Finish Time Selection
                        </Button>
                      </div>
                    </div>
                    <form
                    // onSubmit={formikPartnerBookingPreference.handleSubmit}
                    >
                      {/* <LocalizationProvider dateAdapter={AdapterDayjs}>
                      <DemoContainer
                        components={["DatePicker", "DateTimePicker"]}
                      >
                        <DemoItem label="DateTimePicker">
                          <DateTimePicker
                            defaultValue={today}
                            // shouldDisableMonth={isInCurrentMonth}
                            onChange={handleDateChange}
                            views={[
                              "year",
                              "month",
                              "day",
                              // "hours",
                              // "minutes",
                            ]}
                          />
                        </DemoItem>
                      </DemoContainer>
                    </LocalizationProvider> */}

                      <p>Time slots for </p>

                      {/* {timeslots && (
                      <div className="grid grid-cols-8 gap-4 py-4">
                        {timeslots.map((timeSlot, index) => (
                          <div key={index}>
                            <Button
                              variant="outlined"
                              onClick={() => handleClickTimeslot(timeSlot)}
                              className="hover:bg-slate-600 duration-200 ease-in bg-white p-2 px-2 border rounded-md border-gray-600 hover:text-black"
                            >
                              {timeSlot}
                            </Button>
                          </div>
                        ))}
                      </div>
                    )} */}

                      <div className="flex flex-col gap-3 mt-3">
                        <div>
                          <Button
                            variant="contained"
                            sx={{ backgroundColor: "#242333" }}
                            size="small"
                            //onClick={submitBookingDateTime}
                          >
                            Finish Time Selection
                          </Button>
                        </div>
                      </div>
                    </form>
                  </>
                ) : activeStep === 4 ? (
                  <>
                    <form
                    // onSubmit={formikPartnerBookingPreference.handleSubmit}
                    >
                      {/* <p className="text-sm text-gray-400 py-4 select-none">
                      Lorem ipsum dolor, sit amet consectetur adipisicing
                      elit. Natus, sunt!
                    </p> */}
                      <div className="flex flex-row items-center space-x-20">
                        <Typography
                          sx={{
                            fontWeight: "Light",
                            fontSize: [17, 28],
                            margin: 1,
                          }}
                        >
                          Checkout
                        </Typography>
                        <div className="flex">
                          <div className=" border-r-2  border-gray-700">
                            <Typography
                              sx={{
                                fontSize: 25,
                                marginRight: 1,
                                fontWeight: "bold",
                              }}
                            >
                              {/* 11:45 */}
                              {/* {selectedTimeSlot && selectedTimeSlot} */}
                            </Typography>
                          </div>
                          <div className=" ">
                            <Typography
                              sx={{
                                fontSize: 20,
                                marginLeft: 1,
                                fontWeight: "bold",
                              }}
                            >
                              {/* {selectedBookingDate && selectedBookingDate} */}
                            </Typography>
                            <Typography sx={{ fontSize: 13, marginLeft: 1 }}>
                              Service Duration:
                            </Typography>

                            {/* <div>{totalServiceDuration} hrs</div> */}
                          </div>
                        </div>
                      </div>

                      <div className="flex flex-row gap-20 mt-3">
                        <div>
                          <Button
                            variant="contained"
                            sx={{ backgroundColor: "#242333" }}
                            size="small"
                            // onClick={submitFinalBooking}
                          >
                            Finish Booking
                          </Button>
                        </div>

                        <div>Service Type:</div>
                        <div>Service Location:</div>
                      </div>
                    </form>
                  </>
                ) : null}
              </Typography>
              <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
                <Button
                  color="inherit"
                  disabled={activeStep === 0}
                  onClick={handleBack}
                  sx={{ mr: 1 }}
                >
                  Back
                </Button>
                <Box sx={{ flex: "1 1 auto" }} />
                {/* <Button onClick={handleNext} sx={{ mr: 1 }}>
                  Next
                </Button> */}
                {/* {activeStep !== steps.length &&
                  (completed[activeStep] ? (
                    <Typography
                      variant="caption"
                      sx={{ display: "inline-block" }}
                    >
                      Step {activeStep + 1} already completed
                    </Typography>
                  ) : (
                    <Button onClick={handleComplete}>
                      {completedSteps() === totalSteps() - 1
                        ? "Finish"
                        : "Complete Step"}
                    </Button>
                  ))} */}
              </Box>
            </React.Fragment>
          )}
        </div>
      </Box>
    </>
  );
}

//export default Checkout;

const style_box = {
  //position: "absolute",
  //top: "50%",
  // left: "50%",
  //transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  // boxShadow: 6,
  px: 10,
  color: "white",
  //borderRadius: 2,
  //overflowY: "auto",
  //overflowX: "auto",
  //maxWidth: "80vh",
  //maxHeight: "80vh",
  //width: ["90%", "70%"],
};
