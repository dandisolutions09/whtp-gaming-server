import React, { useState } from "react";
import ImageSlider from "../components/ImageSlider";
import { MdBrightness1 } from "react-icons/md";
import { TbRobot } from "react-icons/tb";
import { MdGames } from "react-icons/md";
import { FaPlus } from "react-icons/fa";
import game11 from "../images/game11.jpg";
import game12 from "../images/game12.jpg";
import game8 from "../images/game8.jpg";
import game9 from "../images/game9.jpg";
import game10 from "../images/game10.jpg";
import game5 from "../images/game5.jpeg";
import { GoHome } from "react-icons/go";
import { useNavigate } from "react-router-dom";
import SearchIcon from "@mui/icons-material/Search";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import Box from "@mui/joy/Box";
import FormLabel from "@mui/joy/FormLabel";
import Radio from "@mui/joy/Radio";
import RadioGroup from "@mui/joy/RadioGroup";
import Sheet from "@mui/joy/Sheet";
import { FaPhoneAlt } from "react-icons/fa";


const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(3),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

export default function ConfigPage() {
  const navigate = useNavigate();
  const handleNav_Home = () => {
    navigate("/");
  };
  const handleNav_gameserver = () => {
    navigate("/gameserver");
  };
  const [isClicked, setIsClicked] = useState(null);

  const handleClick = (index) => {
    setIsClicked(index);
  };
  const slides = [
    {
      url: game8,
      title: [
        { label1: "SETUP RUST SERVER..." },
        { label2: "X BOX SERVERS" },
        { label3: "Rust servers are now available. " },
      ],
      buttons: [{ label: "PC" }, { label: "X BOX" }, { label: "READ MORE" }],
    },
    {
      url: game9,
      title: [
        { label1: "SETUP RUST SERVERS." },
        { label2: "Rust servers are now available." },
        { label3: "Explore new epic and amaizing games" },
      ],

      buttons: [{ label: "RENT NOW" }, { label: "READ MORE" }],
    },

    {
      url: game11,
      title: [
        { label1: "MINECRAFT SERVES" },
        { label2: "Get your serves up and running" },
        { label3: "Explore new epic and amaizing games" },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
    },
  ];

  const containerStyles = {
    width: "100%", // Changed "full" to "100%"
    height: "600px",
  };
  return (
    <div className="relative sm:ml-[300px] ">
      <div className="flex flex-col relative bg-gradient-to-r from-gray-500 to-gray-800 w-full md:w-500px h-full overflow-hidden">
        <div className="relative sm:m-6 m-2 flex flex-row justify-around p-6 sm:space-x-5 bg-[#1b2029] ">
          <div className="flex space-x-1 ">
            <MdBrightness1 size={24} color="#EE9B00" />
            <h1 className=" sm:text-md text-sm font-bold">High performance</h1>
          </div>
          <div className="flex space-x-1 ">
            <TbRobot size={24} color="#EE9B00" />
            <h1 className=" sm:text-md text-sm font-bold">High performance</h1>
          </div>
          <div className="flex space-x-1">
            <FaPlus size={24} color="#EE9B00" />
            <h1 className=" sm:text-md text-sm font-bold">How to setup</h1>
          </div>
        </div>
        <div className="flex flex-col sm:m-6 m-2 justify-center mt-12 bg-[#1b2029] p-3 ">
          <div className="py-3">
            <h1 className="text-xl sm:text-sm md:text-2xl lg:text-3xl font-bold text-[#f5f5f5] ml-2">
              Create your own cloud
            </h1>
            <h1 className="text-sm  text-[#f5f5f5] mt-1 font-bold ml-2">
              Choose your server modpack version
            </h1>

            <div className="flex flex-col sm:flex-row w-full justify-center sm:gap-6 py-8">
              <div
                className="p-5 flex fle-col bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4 rounded-md cursor-pointer hover:border-2 border-orange-500 duration-200 ease-linear"
                style={{ backgroundImage: `url(${game12})` }}
              >
                <div className="z-[10]  mt-[30%] flex flex-col w-full">
                  <h1 className="text-gray-200 text-xl font-bold ">
                    Ceate your own configuration
                  </h1>
                  <h1 className="text-gray-200 text-4xl font-bold py-3">2GB</h1>
                  <button className=" bg-[#EE9B00] p-2 rounded-lg text-gray-800 font-bold">
                    Get server
                  </button>
                </div>
              </div>
              <div
                className="p-5 flex fle-col bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4 rounded-md cursor-pointer hover:border-2 border-orange-500 duration-200 ease-linear"
                style={{ backgroundImage: `url(${game12})` }}
              >
                <div className="z-[10]  mt-[30%] flex flex-col w-full">
                  <h1 className="text-gray-200 text-xl font-bold ">
                    Ceate your own configuration
                  </h1>
                  <h1 className="text-gray-200 text-4xl font-bold py-3">3GB</h1>
                  <button className=" bg-[#EE9B00] p-2 rounded-lg text-gray-800 font-bold">
                    Get server
                  </button>
                </div>
              </div>
              <div
                className="p-5 flex fle-col bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4 rounded-md cursor-pointer hover:border-2 border-orange-500 duration-200 ease-linear"
                style={{ backgroundImage: `url(${game12})` }}
              >
                <div className="z-[10]  mt-[30%] flex flex-col w-full">
                  <h1 className="text-gray-200 text-xl font-bold ">
                    Ceate your own configuration
                  </h1>
                  <h1 className="text-gray-200 text-4xl font-bold py-3">4GB</h1>
                  <button className=" bg-[#EE9B00] p-2 rounded-lg text-gray-800 font-bold">
                    Get server
                  </button>
                </div>
              </div>
              <div
                className="p-5 flex fle-col bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4 rounded-md cursor-pointer hover:border-2 border-orange-500 duration-200 ease-linear"
                style={{ backgroundImage: `url(${game12})` }}
              >
                <div className="z-[10]  mt-[50%] flex flex-col w-full">
                  <h1 className="text-gray-200 text-xl font-bold py-3">
                    Ceate your own configuration
                  </h1>
                  <button className=" bg-[#EE9B00] p-2 rounded-lg text-gray-800 font-bold">
                    Configure
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col gap-4 m-2 p-3 justify-center w-full">
            <Box sx={{ width: "100%" }}>
              <FormLabel
                id="storage-label"
                sx={{
                  mb: 2,
                  fontWeight: "xl",
                  textTransform: "uppercase",
                  fontSize: "xs",
                  letterSpacing: "0.15rem",
                  color: "#fff",
                }}
              >
                Storage
              </FormLabel>
              <RadioGroup
                aria-labelledby="storage-label"
                defaultValue="512GB"
                size="lg"
                sx={{ gap: 1.5, flexDirection: ["col", "row"] }}
              >
                {["512GB", "1TB", "2TB"].map((value) => (
                  <Sheet
                    key={value}
                    sx={{
                      p: 2,
                      borderRadius: "md",
                      boxShadow: "sm",
                    }}
                  >
                    <Radio
                      label={`${value} SSD storage`}
                      overlay
                      disableIcon
                      value={value}
                      slotProps={{
                        label: ({ checked }) => ({
                          sx: {
                            fontWeight: "lg",
                            fontSize: "md",
                            color: checked ? "text.primary" : "text.secondary",
                          },
                        }),
                        action: ({ checked }) => ({
                          sx: (theme) => ({
                            ...(checked && {
                              "--variant-borderWidth": "2px",
                              "&&": {
                                // && to increase the specificity to win the base :hover styles
                                borderColor: theme.vars.palette.primary[500],
                              },
                            }),
                          }),
                        }),
                      }}
                    />
                  </Sheet>
                ))}
              </RadioGroup>
            </Box>
            <div className="flex flex-row justify-center gap-4 py-4">
              <span className="flex flex-row gap-3 ">
                <FaPhoneAlt size={24} />
                <h3 className="text-base sm:text-sm md:text-lg lg:text-2xl font-bold">
                  Support
                </h3>
                <h3 className="text-base sm:text-sm md:text-lg lg:text-2xl font-bold">
                 +11 266 232323
                </h3>
              </span>
            </div>

            <div>
              <Search sx={{ fontFamily: "Play" }}>
                <SearchIconWrapper>
                  <SearchIcon />
                </SearchIconWrapper>
                <StyledInputBase
                  placeholder="Search for a game…"
                  inputProps={{ "aria-label": "search" }}
                />
              </Search>
            </div>
          </div>
        </div>
        <div className="flex flex-col sm:flex-row  justify-start overflow-hidden ">
          <div
            className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
            style={{ backgroundImage: `url(${game12})` }}
          >
            <div className="flex relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest  sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
          <div
            className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] m-4  sm:m-1 rounded-lg"
            style={{ backgroundImage: `url(${game8})` }}
          >
            <div className="flex relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest  sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
          <div
            className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
            style={{ backgroundImage: `url(${game10})` }}
          >
            <div className="flex relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
          <div
            className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
            style={{ backgroundImage: `url(${game11})` }}
          >
            <div className="flex relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
          <div
            className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
            style={{ backgroundImage: `url(${game9})` }}
          >
            <div className="flex  relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm  sm:tracking-wide  tracking-widest  sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
        </div>
        <div className="flex sm:flex-row flex-col justify-start overflow-hidden ">
          <div
            className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
            style={{ backgroundImage: `url(${game12})` }}
          >
            <div className="flex relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest  sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
          <div
            className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] m-4  sm:m-1 rounded-lg"
            style={{ backgroundImage: `url(${game8})` }}
          >
            <div className="flex relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest  sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
          <div
            className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
            style={{ backgroundImage: `url(${game10})` }}
          >
            <div className="flex relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
          <div
            className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
            style={{ backgroundImage: `url(${game11})` }}
          >
            <div className="flex relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
          <div
            className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
            style={{ backgroundImage: `url(${game9})` }}
          >
            <div className="flex  relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm  sm:tracking-wide  tracking-widest  sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
        </div>
      </div>
    </div>
  );
}
