import React, { useEffect, useState } from "react";
import ImageSlider from "../components/ImageSlider";
import { MdBrightness1 } from "react-icons/md";
import { TbRobot } from "react-icons/tb";
import { MdGames } from "react-icons/md";
import { FaPlus } from "react-icons/fa";
import game11 from "../images/game11.jpg";
import test from "../assets/image_test.png";
import game12 from "../images/game12.jpg";
import rust from "../images/game8.jpg";
import game9 from "../images/game9.jpg";
import game10 from "../images/game10.jpg";
import game5 from "../images/game5.jpeg";

// import space_engineers from "../assets/space_engineers.jpg";
import space_engineers from "../assets/se_3.jpg";

import minecraft_pic from "../assets/mine_craft.jpg";
import { Backdrop, Box, Button, Fade, Modal, Typography } from "@mui/material";
import { useGoogleLogin } from "@react-oauth/google";
import axios from "axios";
import GeneralSuccess from "../components/GeneralSuccess";
import endpoints from "../endpoints";
import { useNavigate } from "react-router-dom";
import Navbar from "../components/Navbar";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const handleRentRustClick = () => {
  // Handle "RENT RUST" button click
  console.log("Rent Rust button clicked");
  // Add your custom logic here
};

const handleReadMoreClick = () => {
  // Handle "READ MORE" button click
  console.log("Read More button clicked");
  // Add your custom logic here
};

export default function HomePage() {
  const [open, setOpen] = React.useState(false);
  const handleClose = () => setOpen(false);

  const [errorMsg, setErrorMsg] = useState();
  const [successMsg, setSuccessMsg] = useState();

  const [generalError, setGeneralError] = useState();
  const [generalSuccess, setGeneralSuccess] = useState();

  const navigate = useNavigate();
  const [isDropdownOpen, setIsDropdownOpen] = useState(false); // State to toggle dropdown visibility
  const [selectedOption, setSelectedOption] = useState(""); // State for the selected option

  const [usr_obj, setUserObj] = useState();

  const toggleDropdown = () => {
    setIsDropdownOpen((prev) => !prev); // Toggle dropdown visibility
  };

  const handleOptionClick = (option) => {
    setSelectedOption(option); // Update selected option
    setIsDropdownOpen(false); // Close dropdown after selection
  };

  const postUserRegistrationData = async (data) => {
    const visitorRegistrationObject = {
      email: data.email.trim(),
      first_name: data.given_name,
      last_name: data.family_name,
      profile_pic: data.picture,
    };

    // localStorage.setItem("user", visitorRegistrationObject);

    console.log("main object", visitorRegistrationObject);

    const storedUserState = localStorage.getItem("isRegistered");

    try {
      //setLoading(true);
      const response = await axios.post(
        endpoints.visitorLogin,
        visitorRegistrationObject
      );

      console.log("Response FROM BACKEND", response.data.data.user_state);

      if (response.data.data.user_state == "existing") {
        console.log("user exists");

        localStorage.setItem(
          "user",
          JSON.stringify(response.data.data.partner)
        );

        

        localStorage.setItem("user-id", response.data.data.partner._id);

        navigate("/dashboard");

        return;
      }

      localStorage.setItem("user", visitorRegistrationObject);

      console.log("finall", visitorRegistrationObject);

      localStorage.setItem("user", JSON.stringify(visitorRegistrationObject));

      const storedUser = JSON.parse(localStorage.getItem("user"));

      console.log("STORED USER email", storedUser.email);

      navigate("/game-pricing");

      // } else if (response.data.data.user_state == "new_user") {
      //   console.log("new user");
      // }

      //console.log("Response from the backend:", response.data.data.partner._id);

      localStorage.setItem("user-id", response.data.data.partner._id);

      //localStorage.setItem("user_id", JSON.stringify(response.data.partner._id));
      //localStorage.setItem("user_id", JSON.stringify(gameObject));

      //localStorage.setItem("user", JSON.stringify(visitorRegistrationObject));

      localStorage.setItem("user_id", response.data.data.partner._id);
    } catch (error) {
      console.error("Error sending data to the backend:", error);
      console.log("Error detected");
      setErrorMsg("Error sending data to the backend");
      setGeneralError(true);
      return;
    }
    // } finally {

    //   //setLoading(false);
    // }
  };

  const handleRentRustClick = () => {
    // Handle "RENT RUST" button click
    console.log("Rent Rust button clicked");

    const userID = localStorage.getItem("user-id");

    console.log("userD", userID);
    //setOpen(true);

    if (!userID) {
      console.log("user not registered!");

      setOpen(true);
    } else {
      navigate("/dashboard");
    }

    //   // const timer = setTimeout(() => {
    //   //   setOpen(true);
    //   // }, 5000); // 10 seconds
    //   return () => clearTimeout(timer);
    // } else {
    //   console.log("user already registered!");
    // }
    //setOpen(true);

    localStorage.setItem("user-game", "Rust Survival");

    const gameObject = {
      game_name: "Rust Survival",
      game_pic_url: rust,
      // last_name: data.family_name,
      //profile_pic: data.picture,
    };

    localStorage.setItem("game-object", JSON.stringify(gameObject));

    //navigate("/game-pricing");

    // Add your custom logic here
  };

  const login = useGoogleLogin({
    onSuccess: async (response) => {
      try {
        const res = await axios.get(
          "https://www.googleapis.com/oauth2/v3/userinfo",
          {
            headers: {
              Authorization: `Bearer ${response.access_token}`,
            },
          }
        );

        console.log(res);

        postUserRegistrationData(res.data);

        setOpen(false);
        setSuccessMsg("Success!");
        setGeneralSuccess(true);

        //window.location.href = "https://buy.stripe.com/test_eVa8yc3hz8C2c4UeUU";
      } catch (err) {
        console.log(err);
      }
    },
  });
  const slides = [
    {
      url: game9,
      title: [
        { label1: "SETUP RUST SERVER..." },
        { label2: "X BOX SERVERS" },
        { label3: "Survive the harsh wilderness on our Rust server" },
      ],
      // buttons: [{ label: "RENT RUST", onClick: handleRentRustClick }, { label: "READ MORE" }],
      buttons: [
        { label: "GET STARTED", onClick: handleRentRustClick },
        { label: "READ MORE", onClick: handleReadMoreClick },
      ],
    },
    {
      url: space_engineers,
      title: [
        { label1: "SPACE ENGINEERS SERVERS." },
        { label2: "Space Engineers servers are now available." },
        {
          label3: "Embark on a thrilling journey in our Space Engineers server",
        },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
    },

    {
      url: minecraft_pic,
      title: [
        { label1: "MINECRAFT SERVERS" },
        { label2: "Get your servers up and running" },
        {
          label3:
            "Join our Minecraft server for an epic survival adventure with custom plugins, friendly community, and exciting events!",
        },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
    },
  ];

  const containerStyles = {
    width: "100%", // Changed "full" to "100%"
    height: "700px",
  };

  useEffect(() => {
    const storedUser = JSON.parse(localStorage.getItem("user"));

    console.log("stored user", storedUser);
    setUserObj(storedUser);

    const userID = localStorage.getItem("user-id");

    // setOpen(true);--> for testing purposes

    if (!userID) {
      console.log("user not registered!");

      setOpen(true);
    } else {
      navigate("/dashboard");
    }

    // Cleanup when the component unmounts
    return () => {
      // eventSource.close();
    };
  }, []);

  return (
    <div>
      <div>
        <Navbar />
        {/* <Button onClick={handleOpen}>Open modal</Button> */}

        <GeneralSuccess
          open={generalSuccess}
          autoHideDuration={6000}
          //name={"Success"}
          msg={successMsg}
          onClose={() => setGeneralSuccess(false)}
        />
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          open={open}
          onClose={handleClose}
          closeAfterTransition
          slots={{ backdrop: Backdrop }}
          slotProps={{
            backdrop: {
              timeout: 500,
            },
          }}
        >
          <Fade in={open}>
            <Box sx={style}>
              {/* <Typography
                id="transition-modal-title"
                variant="h6"
                component="h2"
              >
                Join Us!
              </Typography> */}

              <h1 className="text-3xl text-blue-900">Join Us!</h1>
              <Typography id="transition-modal-description" sx={{ mt: 2 }}>
                {/* Welcome to our application! Please sign in with your Google
                account to continue. */}
                <h1 className="text- text-blue-900">
                  Welcome! Please sign in with your Google account to continue.
                </h1>
              </Typography>

              <div className="p-4">
                <button
                  onClick={() => login()}
                  className="px-6 p-3 rounded-full text-md text-white/90 bg-[#001b5e] text-sm font-bold"
                >
                  Sign in with Google
                </button>
              </div>
            </Box>
          </Fade>
        </Modal>
      </div>
      <div style={containerStyles}>
        <ImageSlider slides={slides} />
      </div>

      {/* <div className="flex flex-col relative bg-gradient-to-r from-gray-800 to-gray-500 w-full md:w-500px"> */}
      {/* <Button>Login</Button> */}

      <div className="flex flex-col relative bg-gradient-to-r from-gray-950 to-gray-950 w-full md:w-500px">
        <div className="flex flex-col m-6 justify-center mt-12">
          <h1 className="text-base sm:text-sm md:text-lg lg:text-3xl font-bold text-[#f5f5f5] lg:ml-[23px] md:ml-[50px]   sm:ml-[280px] ml-[10px] text-center">
            DISCOVER PREMIUM GAMESERVER HOSTING FROM WHTP
          </h1>
          <div className="flex flex-row gap-6 m-2 p-3 justify-center">
            <span className="flex flex-row gap-1 hover:underline duration-200 ease-in cursor-pointer decoration-orange-500 underline-offset-8 decoration-4 ">
              <FaPlus size={24} />
              <h3 className="text-base sm:text-sm md:text-lg lg:text-xl">
                OUR TOP GAMES
              </h3>
            </span>
            <span className="flex flex-row gap-1 hover:underline cursor-pointer decoration-orange-500 underline-offset-8 decoration-4">
              <MdGames size={24} />
              <h3 className="text-base sm:text-sm md:text-lg lg:text-xl">
                {" "}
                OUR TOP GAMES
              </h3>
            </span>
          </div>
        </div>
        <div className="flex sm:flex-row flex-col sm:h-64 justify-center">
          <div
            className="bg-cover bg-center sm:w-[430px] h-[256px] sm:m-1 m-4  rounded-lg"
            style={{ backgroundImage: `url(${game12})` }}
          >
            <div className="flex relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest  sm:p-1 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
          <div
            className="bg-cover bg-center sm:w-[430px] h-[256px] sm:m-1 m-4 rounded-lg"
            style={{ backgroundImage: `url(${rust})` }}
          >
            <div className="flex relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest  sm:p-1 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
          <div
            className="bg-cover bg-center sm:w-[430px] h-[256px] sm:m-1 m-4  rounded-lg"
            style={{ backgroundImage: `url(${game10})` }}
          >
            <div className="flex relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest sm:p-1 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
          <div
            className="bg-cover bg-center sm:w-[430px] h-[256px] sm:m-1 m-4  rounded-lg"
            style={{ backgroundImage: `url(${game11})` }}
          >
            <div className="flex relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest sm:p-1 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
          <div
            className="bg-cover bg-center sm:w-[430px] h-[256px] sm:m-1 m-4  rounded-lg"
            style={{ backgroundImage: `url(${game9})` }}
          >
            <div className="flex  relative top-[80%] justify-center">
              <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm  sm:tracking-wide  tracking-widest  sm:p-1 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                ORDER NOW
              </button>
            </div>
            <h1 className="flex relative top-[55%] justify-center font-bold">
              From $1.65
            </h1>
          </div>
        </div>
      </div>
    </div>
  );
}
