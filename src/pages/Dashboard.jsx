import React, { useEffect, useState } from "react";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

import rust from "../assets/rust.jpg";

import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { IoIosSettings, IoMdCloseCircle } from "react-icons/io";
import { IoLogoPaypal } from "react-icons/io5";
import { RiErrorWarningLine, RiVisaFill } from "react-icons/ri";

import {
  CircularProgress,
  FormControl,
  IconButton,
  InputLabel,
  LinearProgress,
  MenuItem,
  Select,
  StepButton,
} from "@mui/material";
import { RiAccountPinCircleFill } from "react-icons/ri";
import { HiServer } from "react-icons/hi";
import { FaCcMastercard, FaPlay, FaStop } from "react-icons/fa";
import { SiAmericanexpress } from "react-icons/si";
import { AiOutlineCheck } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import Navbar from "../components/Navbar";
import { ImStatsDots } from "react-icons/im";
import { FaUsers } from "react-icons/fa6";
import endpoints from "../endpoints";
import axios from "axios";
import ContainerStatus from "../components/ContainerStatus";
import GeneralError from "../Alerts/GeneralError";
import GeneralSuccess from "../components/GeneralSuccess";

const steps = ["Game Picked", "Server Selected", "Buy", "Set up and Play"];

export default function Checkout() {
  const navigate = useNavigate();
  const [activeStep, setActiveStep] = React.useState(2);
  const [skipped, setSkipped] = React.useState(new Set());
  //const [completed, setCompleted] = React.useState({});
  const [completed, setCompleted] = useState({ 0: true, 1: true }); // Step 1 and Step 2 are marked complete
  const [selected, setSelected] = useState(null);

  const [game_name, setGameName] = useState("");

  const [game_slots, setSlots] = useState("");

  const [game_pic, setGamePic] = useState("");

  const [res, setResponse] = useState("");

  const [userID_state_object, setUserID] = useState("");

  const [msgs, setMessages] = useState();

  const [generalError, setGeneralError] = useState();
  const [errorMsg, setErrorMsg] = useState();

  const [generalSuccess, setGeneralSuccess] = useState();
  const [successMsg, setSuccessMsg] = useState();
  const [loading, setLoading] = useState();

  // const navigate = useNavigate();

  const handleSelect = (paymentType) => {
    setSelected(paymentType);
  };

  const isStepOptional = (step) => {
    return step === 1;
  };

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleComplete = () => {
    const newCompleted = completed;
    newCompleted[activeStep] = true;
    console.log("active step", activeStep);

    console.log("new completed", newCompleted[activeStep]);
    setCompleted(newCompleted);
    handleNext();
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleChoosePackageButton = () => {
    // Handle "RENT RUST" button click
    console.log("Rent Rust button clicked");
    //setOpen(true);

    navigate("/dashboard");

    // Add your custom logic here
  };

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("You can't skip a step that isn't optional.");
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values());
      newSkipped.add(activeStep);
      return newSkipped;
    });
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const handleClose_EditProvider = () => {
    setOpenEdit(false);
  };

  const startServer = async () => {
    const user = localStorage.getItem("user_id");

    //const user_id = user._id;
    console.log("user", user);
    try {
      // Assuming the response contains data
      // `${endpoints.editUserInfo}?partner_id=${usr_id}`
      const response = await axios.get(
        `${endpoints.starServer}?user_id=${user}`
      );
    } catch (err) {
      //setError("Failed to fetch data");
      console.error(err); // Log the error for debugging
    }
  };

  const handleNavigateToServer = (game_name) => {
    console.log("game name", game_name);
    // startServer();

    localStorage.setItem("game_name_for_settings", game_name);

    navigate("/server-settings");
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const totalSteps = () => {
    return steps.length;
  };

  const completedSteps = () => {
    return Object.keys(completed).length;
  };

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps();
  };

  // function checkIdAndStatus(array, id, game_name) {
  //   // Check if the id exists and the status is "running"
  //   const found = array.find((obj) => obj.name.includes(id));

  //   // Return true if found and its status is "running", otherwise return false
  //   return found ? found.status === "running" : false;
  // }

  function checkIdAndStatus(array, id, game_name) {
    // Check if the id and game_name exist and the status is "running"
    const found = array.find(
      (obj) => obj.name.includes(id) && obj.game_name === game_name
    );

    // Return true if found and its status is "running", otherwise return false
    return found ? found.status === "running" : false;
  }

  const handleStartMinecraft = (userID) => {
    console.log("start minecraft server", userID);
    console.log("messages", msgs);
    const game_name = "Minecraft";

    const exists = checkIdAndStatus(msgs, userID, game_name);

    console.log("exists", exists);

    setGeneralSuccess(true);
    setSuccessMsg("Starting Server please wait...");

    if (exists) {
      setGeneralError(true);
      setErrorMsg("Minecraft is already running!!");
      return;
    } else if (!exists) {
      const game_name = "minecraft";
      axios
        .post(`${endpoints.starServer}?game=${game_name}&uid=${userID}`)
        .then((res) => {
          console.log(res.data);
          //setResponse(res.data); // Save response data
        })
        .catch((error) => {
          console.error("There was an error!", error);
        });
    }
  };

  const handleStopMinecraft = (userID) => {
    setGeneralSuccess(true);
    setSuccessMsg("Stopping Server please wait...");

    console.log("stop minecraft server", userID);

    const game_name = "minecraft";
    axios
      .post(`${endpoints.stopServer}?game=${game_name}&uid=${userID}`)
      .then((res) => {
        console.log(res.data);
        //setResponse(res.data); // Save response data
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };

  const handleStopRust = (userID) => {
    setGeneralSuccess(true);
    setSuccessMsg("Stopping Server please wait...");

    console.log("stop minecraft server", userID);

    const game_name = "rust";
    axios
      .post(`${endpoints.stopServer}?game=${game_name}&uid=${userID}`)
      .then((res) => {
        console.log(res.data);
        //setResponse(res.data); // Save response data
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };

  //Start Rust
  const handleStartRust = (userID) => {
    console.log("start rust server", userID);
    console.log("messages", msgs);

    const game_name = "Rust";

    const exists = checkIdAndStatus(msgs, userID, game_name);

    console.log("exists", exists);

    if (exists) {
      setGeneralError(true);
      setErrorMsg("Rust is already running!!");
      return;
    } else if (!exists) {
      const game_name = "rust";
      axios
        .post(`${endpoints.starServer}?game=${game_name}&uid=${userID}`)
        .then((res) => {
          console.log(res.data);
          //setResponse(res.data); // Save response data
        })
        .catch((error) => {
          console.error("There was an error!", error);
        });
    }
  };

  // const formatContainerStats = (data, user_id) => {
  //   // Split the raw data by line breaks
  //   const lines = data.trim().split("\\nContainer ID: ");

  //   // Map each line to an object with container details
  //   return lines
  //     .map((line, index) => {
  //       const parts =
  //         index === 0
  //           ? line.split("|")
  //           : ["Container ID: " + line].join("|").split("|");

  //       // Trim and clean up Container ID, Name, and Status
  //       const containerId = parts[0]
  //         ?.replace("Container ID:", "")
  //         .replace(/["\\]/g, "")
  //         .trim();
  //       const name = parts[1]?.replace("Name:", "").trim();
  //       const status = parts[2]?.replace("Status:", "").trim();

  //       // Return the object only if the name contains user_id
  //       if (name && user_id && name.includes(user_id)) {
  //         return { containerId, name, status };
  //       }

  //       // Return null or undefined if the name does not contain user_id
  //       return null;
  //     })
  //     .filter(Boolean); // Filter out null or undefined values
  // };

  const formatContainerStats = (data, user_id) => {
    // Split the raw data by line breaks
    const lines = data.trim().split("\\nContainer ID: ");

    // Map each line to an object with container details
    return lines
      .map((line, index) => {
        const parts =
          index === 0
            ? line.split("|")
            : ["Container ID: " + line].join("|").split("|");

        // Trim and clean up Container ID, Name, Status, and Ports
        const containerId = parts[0]
          ?.replace("Container ID:", "")
          .replace(/["\\]/g, "")
          .trim();
        const name = parts[1]?.replace("Name:", "").trim();
        const status = parts[2]?.replace("Status:", "").trim();
        const ports = parts[3]?.replace("Ports:", "").trim(); // Extract ports

        // Return the object only if the name contains user_id
        if (name && user_id && name.includes(user_id)) {
          return { containerId, name, status, ports }; // Include ports in the returned object
        }

        // Return null or undefined if the name does not contain user_id
        return null;
      })
      .filter(Boolean); // Filter out null or undefined values
  };

  const menuItems = [
    { value: "Kenya", label: "Kenya" },
    { value: "United States", label: "United States" },
    // { value: "INACTIVE", label: "INACTIVE" },
    // { value: "FEATURED", label: "FEATURED" },
  ];

  useEffect(() => {
    setLoading(true);
    const userPackage = JSON.parse(localStorage.getItem("user-package"));

    const storedUser = JSON.parse(localStorage.getItem("user"));
    console.log("stored user", storedUser);

    const gameObject = JSON.parse(localStorage.getItem("game-object"));

    // // Find the specific Minecraft container
    // const minecraftContainer = msgs.find((container) =>
    //   container.name.includes("Minecraft")
    // );

    // // Check if the Minecraft container is running
    // const isMinecraftContainerRunning =
    //   minecraftContainer && minecraftContainer.status === "running";

    const user_id = localStorage.getItem("user-id");

    console.log("user ID", user_id);
    setUserID(user_id);

    // Create a new EventSource connection
    const eventSource = new EventSource(
      //`${endpoints.getContainerStats}?user_id=${user_id}&game_name=Minecraft`

      `${endpoints.getContainerStats}?user_id=${user_id}`
    );

    // Listen for messages from the server
    eventSource.onmessage = (event) => {
      // console.log("events data", event.data);

      const formattedData = formatContainerStats(event.data, user_id);
      console.log("formattedData", formattedData);
      setMessages(formattedData);

      setLoading(false);
    };

    // Handle errors
    eventSource.onerror = (error) => {
      console.error("EventSource failed:", error);
      eventSource.close();
    };

    // Cleanup when the component unmounts
    return () => {
      eventSource.close();
    };
  }, []);

  return (
    <>
      <Navbar />

      {loading && (
        <div>
          <CircularProgress color="success" />
        </div>
      )}

      <GeneralError
        open={generalError}
        autoHideDuration={6000}
        Errmsg={errorMsg}
        onClose={() => setGeneralError(false)}
      />

      <GeneralSuccess
        open={generalSuccess}
        autoHideDuration={6000}
        //name={"Success"}
        msg={successMsg}
        onClose={() => setGeneralSuccess(false)}
      />

      <div className="bg-gradient-to-r from-gray-950 to-gray-950">
        <div className="flex justify-center h-screen relative  ">
          <div className="flex flex-col">
            <div className="flex flex-col gap-10 pt-2">
              <div className="text-center font-bold text-4xl">Dashboard</div>
              <div className="flex space-x-8">
                {/* Box 1 */}
                <div className="border border-gray-400 w-[620px] h-[220px] flex flex-col justify-between relative p-4">
                  <div className="flex flex-col items-center justify-center h-full">
                    <h2 className="text-4xl text-center">Minecraft</h2>
                    <h2 className="text-lg text-center mt-4">
                      50.4.71.78:6865
                    </h2>
                  </div>

                  {/* Conditional rendering based on the name */}
                  <div className="absolute top-0 right-0 text-xs m-2">
                    {msgs &&
                      msgs
                        .filter((container) =>
                          container.name.includes("Minecraft")
                        ) // Filter containers with "Rust" in the name
                        .map((container) => (
                          <p
                            className={
                              container.status === "running"
                                ? "text-green-500"
                                : "text-red-500"
                            } // Conditional class based on status
                            key={container.containerId}
                          >
                            {container.status}{" "}
                            {/* Render status for filtered containers */}
                          </p>
                        ))}
                  </div>

                  <div className="absolute top-0 left-0 text-xs m-2">
                    0/10 players
                  </div>

                  {/* <div className="absolute top-0 right-0 text-xs m-2">
                    stopped...
                  </div> */}

                  <div className="flex flex-row gap-2">
                    {/* Conditionally render the button only if no container is running */}
                    {msgs &&
                      !msgs.some(
                        (container) =>
                          container.status === "running" &&
                          container.name.includes("Minecraft")
                      ) && (
                        <button
                          className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                          onClick={() => handleChoosePackageButton()}
                        >
                          <div
                            className="flex flex-row gap-2 items-center"
                            onClick={() =>
                              handleStartMinecraft(userID_state_object)
                            }
                          >
                            <FaPlay />
                            start
                          </div>
                        </button>
                      )}

                    {msgs &&
                      !msgs.some(
                        (container) =>
                          container.status === "exited" &&
                          container.name.includes("Minecraft")
                      ) && (
                        <button
                          className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                          onClick={() => handleChoosePackageButton()}
                        >
                          <div
                            className="flex flex-row gap-2 items-center"
                            onClick={() =>
                              handleStopMinecraft(userID_state_object)
                            }
                          >
                            <FaStop />
                            stop
                          </div>
                        </button>
                      )}

                    {/* <button
                      className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                      onClick={() => handleChoosePackageButton()}
                    >
                      <div
                        className="flex flex-row gap-2 items-center"
                        onClick={() => handleStopMinecraft(userID_state_object)}
                      >
                        <FaStop />
                        stop
                      </div>
                    </button> */}

                    {msgs &&
                      msgs.length > 0 &&
                      !msgs.some(
                        (container) =>
                          container.status === "exited" &&
                          container.name.includes("Minecraft")
                      ) && (
                        <button
                          className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                          onClick={() => handleNavigateToServer("Minecraft")}
                        >
                          <div
                            className="flex flex-row gap-2 items-center"
                            onClick={() =>
                              handleNavigateToServer(userID_state_object)
                            }
                          >
                            <IoIosSettings />
                            more
                          </div>
                        </button>
                      )}

                    {/* <button
                      className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                      onClick={() => handleNavigateToServer("Minecraft")}
                    >
                      <div className="flex flex-row gap-2 items-center">
                        <IoIosSettings />
                        more
                      </div>
                    </button> */}
                  </div>
                </div>

                {/* Box 2 */}

                <div className="border border-gray-400 w-[620px] h-[220px] flex flex-col justify-between relative p-4">
                  <div className="flex flex-col items-center justify-center h-full">
                    <h2 className="text-4xl text-center">Rust</h2>
                    <h2 className="text-lg text-center mt-4">
                      50.4.71.78:6865
                    </h2>
                  </div>
                  <div className="absolute top-0 right-0 text-xs m-2">
                    <div className="absolute top-0 right-0 text-xs m-2">
                      {msgs &&
                        msgs
                          .filter((container) =>
                            container.name.includes("Rust")
                          ) // Filter containers with "Rust" in the name
                          .map((container) => (
                            <p
                              className={
                                container.status === "running"
                                  ? "text-green-500"
                                  : "text-red-500"
                              } // Conditional class based on status
                              key={container.containerId}
                            >
                              {container.status}{" "}
                              {/* Render status for filtered containers */}
                            </p>
                          ))}
                    </div>
                  </div>
                  <div className="absolute top-0 left-0 text-xs m-2">
                    0/10 players
                  </div>

                  <div className="flex flex-row gap-2">
                    {msgs &&
                      !msgs.some(
                        (container) =>
                          container.status === "running" &&
                          container.name.includes("Rust")
                      ) && (
                        <button
                          className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                          onClick={() => handleChoosePackageButton()}
                        >
                          <div
                            className="flex flex-row gap-2 items-center"
                            onClick={() => handleStartRust(userID_state_object)}
                          >
                            <FaPlay />
                            start
                          </div>
                        </button>
                      )}

                    {msgs &&
                      !msgs.some(
                        (container) =>
                          container.status === "exited" &&
                          container.name.includes("Rust")
                      ) && (
                        <button
                          className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                          onClick={() => handleChoosePackageButton()}
                        >
                          <div
                            className="flex flex-row gap-2 items-center"
                            onClick={() => handleStopRust(userID_state_object)}
                          >
                            <FaStop />
                            stop
                          </div>
                        </button>
                      )}

                    {msgs &&
                      msgs.length > 0 &&
                      !msgs.some(
                        (container) =>
                          container.status === "exited" &&
                          container.name.includes("Rust")
                      ) && (
                        <button
                          className="mt-auto bg-[#EE9B00] text-black px-4 py-2 rounded"
                          onClick={() => handleNavigateToServer("Rust")}
                        >
                          <div
                            className="flex flex-row gap-2 items-center"
                            onClick={() =>
                              handleNavigateToServer(userID_state_object)
                            }
                          >
                            <IoIosSettings />
                            more
                          </div>
                        </button>
                      )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

//export default Checkout;

const style_box = {
  //position: "absolute",
  //top: "50%",
  // left: "50%",
  //transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  // boxShadow: 6,
  px: 4,
  color: "white",
  height: "100vh",
  //borderRadius: 2,
  //overflowY: "auto",
  //overflowX: "auto",
  //maxWidth: "80vh",
  //maxHeight: "80vh",
  //width: ["90%", "70%"],
};
