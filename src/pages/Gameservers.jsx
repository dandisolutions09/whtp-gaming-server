import React, { useState } from "react";
import ImageSlider from "../components/ImageSlider";
import { MdBrightness1 } from "react-icons/md";
import { TbRobot } from "react-icons/tb";
import { MdGames } from "react-icons/md";
import { FaPlus } from "react-icons/fa";
import game11 from "../images/game11.jpg";
import game12 from "../images/game12.jpg";
import game8 from "../images/game8.jpg";
import game9 from "../images/game9.jpg";
import game10 from "../images/game10.jpg";
import game5 from "../images/game5.jpeg";
import { GoHome } from "react-icons/go";
import { useNavigate } from "react-router-dom";
import SearchIcon from "@mui/icons-material/Search";
import { styled, alpha } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  Modal,
  TextField,
  Typography,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { useFormik } from "formik";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(3),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

export default function Gameservers() {
  const [openRustModal, setopenRustModal] = useState(false);

  const navigate = useNavigate();
  const handleNav_Home = () => {
    navigate("/");
  };

  const handleClickOrderNow = (index) => {
    // setIsClicked(index);
    console.log(`Rent Server button clicked for slide index ${index}`);

    setopenRustModal(true);
    // Add your functionality here
  };

  const handleClostRustModal = (index) => {
    // setIsClicked(index);
    console.log(`Rent Server button clicked for slide index ${index}`);

    setopenRustModal(false);
    // Add your functionality here
  };

  //
  const slides = [
    {
      url: game8,
      title: [
        { label1: "SETUP RUST SERVER..." },
        { label2: "X BOX SERVERS" },
        { label3: "Rust servers are now available. " },
      ],
      buttons: [{ label: "PC" }, { label: "X BOX" }, { label: "READ MORE" }],
    },
    {
      url: game9,
      title: [
        { label1: "SETUP RUST SERVERS." },
        { label2: "Rust servers are now available." },
        { label3: "Explore new epic and amaizing games" },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
    },

    {
      url: game11,
      title: [
        { label1: "MINECRAFT SERVES" },
        { label2: "Get your serves up and running" },
        { label3: "Explore new epic and amaizing games" },
      ],

      buttons: [{ label: "RENT SERVER" }, { label: "READ MORE" }],
    },
  ];

  const containerStyles = {
    width: "100%", // Changed "full" to "100%"
    height: "600px",
  };

  const postData = async (data_) => {
    // let installment_array = [];
    console.log("data to be sent", data_);
    // records_array.push(data_);

    //console.log("new incoming installments", incoming_instalments);

    // var storedDataJSON = localStorage.getItem("userData_storage");
    // var storedData = JSON.parse(storedDataJSON);

    try {
      const response = await fetch(
        `http://localhost:8081/submit-rust-config`,

        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data_),
        }
      );

      if (response.ok) {
        console.log("Records updated successfully");
       // setOpenRecordAddedSuccessfully(true);

        //setOpenRecordChangeSuccessfully(true);
       // setRefresher(true);

        // setTimeout(() => {
        //   setOpenBooking(false);
        //   reloadPage();
        // }, 2000);
        //setOpenRecordAddedSuccessfully(true);
      } else {
        console.error("Failed to update record information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const formikMain = useFormik({
    initialValues: {
      server_name: "",
      server_banner: "",
      server_identity: "",
      server_description: "",
      max_players: "",
      world_size: "",
      seed: "",

      // client_email: "",
      // client_nat_id: "",
    },

    onSubmit: (values) => {
      console.log("values", values);
      postData(values);
    },

    // setTimeout(postData(signupInformationRef.current), 3000)

    // sendMessage("facility_data", values);
    // navigate("/facilities");
    //},
  });
  return (
    <>
      <Modal
        open={openRustModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <div className="flex flex-row justify-between">
            <Typography
              id="modal-modal-title"
              sx={{
                fontWeight: "bold",
                fontSize: ["14px", "18px"],
                color: "black",
              }}
            >
              Rust Server Configuration
            </Typography>
            {/* <h2 className="font-bold m-4"> Add Record </h2> */}
            <CloseIcon
              fontSize="40"
              onClick={handleClostRustModal}
              sx={{ cursor: "pointer" }}
            />
          </div>
          <form onSubmit={formikMain.handleSubmit}>
            <div className="flex flex-col gap-4 ">
              <div className="flex flex-col gap-3">
                <Typography
                  id="modal-modal-title"
                  sx={{ fontWeight: "bold", fontSize: ["14px", "16px"] }}
                >
                  Client's Details
                </Typography>
                <div className="flex flex-col gap-3">
                  <div className="flex flex-row gap-3 mt-3">
                    <TextField
                      type="text"
                      id="server_name"
                      name="server_name"
                      label="Rust Server Name"
                      placeholder="Your Rust Server Name"
                      value={formikMain.values.server_name}
                      error={
                        formikMain.touched.server_name &&
                        Boolean(formikMain.errors.server_name)
                      }
                      helperText={
                        formikMain.touched.server_name &&
                        formikMain.errors.server_name
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    />

                    <TextField
                      type="text"
                      id="server_banner"
                      name="server_banner"
                      label="Server Banner"
                      placeholder="Server Banner"
                      value={formikMain.values.server_banner}
                      error={
                        formikMain.touched.server_banner &&
                        Boolean(formikMain.errors.server_banner)
                      }
                      helperText={
                        formikMain.touched.server_banner &&
                        formikMain.errors.server_banner
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      //onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    />

                    <TextField
                      type="text"
                      id="server_identity"
                      name="server_identity"
                      label="Server Identity"
                      placeholder="Server Identity"
                      value={formikMain.values.server_identity}
                      error={
                        formikMain.touched.server_identity &&
                        Boolean(formikMain.errors.server_identity)
                      }
                      helperText={
                        formikMain.touched.server_identity &&
                        formikMain.errors.server_identity
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      //onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    />
                  </div>

                  <TextField
                    type="text"
                    id="server_description"
                    name="server_description"
                    label="Server Description"
                    placeholder="Server Description"
                    value={formikMain.values.server_description}
                    error={
                      formikMain.touched.server_description &&
                      Boolean(formikMain.errors.server_description)
                    }
                    helperText={
                      formikMain.touched.server_description &&
                      formikMain.errors.server_description
                    }
                    onChange={(e) => {
                      e.target.value = e.target.value.toUpperCase();
                      formikMain.handleChange(e);
                    }}
                    //onBlur={formikMain.handleBlur}
                    // sx={{
                    //   width: ["100%", "50%"], // Adjust width for different screen sizes
                    //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                    // }}
                  />

                  <div className="flex flex-row gap-3 ">
                    <TextField
                      type="text"
                      id="max_players"
                      name="max_players"
                      label="Max Players"
                      placeholder="Max Players"
                      value={formikMain.values.max_players}
                      error={
                        formikMain.touched.max_players &&
                        Boolean(formikMain.errors.max_players)
                      }
                      helperText={
                        formikMain.touched.max_players &&
                        formikMain.errors.max_players
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      //onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    />

                    <TextField
                      type="text"
                      id="world_size"
                      name="world_size"
                      label="World Size"
                      placeholder="World Size"
                      value={formikMain.values.world_size}
                      error={
                        formikMain.touched.world_size &&
                        Boolean(formikMain.errors.world_size)
                      }
                      helperText={
                        formikMain.touched.world_size &&
                        formikMain.errors.world_size
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      //onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    />

                    <TextField
                      type="text"
                      id="seed"
                      name="seed"
                      label="Server Seed"
                      placeholder="Server Seed"
                      value={formikMain.values.seed}
                      error={
                        formikMain.touched.seed &&
                        Boolean(formikMain.errors.seed)
                      }
                      helperText={
                        formikMain.touched.seed && formikMain.errors.seed
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      //onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    />
                    {/* <TextField
                      type="text"
                      id="client_lastname"
                      name="client_lastname"
                      label="Last Name"
                      placeholder="Last Name"
                      value={formikMain.values.client_lastname}
                      error={
                        formikMain.touched.client_lastname &&
                        Boolean(formikMain.errors.client_lastname)
                      }
                      helperText={
                        formikMain.touched.client_lastname &&
                        formikMain.errors.client_lastname
                      }
                      onChange={(e) => {
                        e.target.value = e.target.value.toUpperCase();
                        formikMain.handleChange(e);
                      }}
                      onBlur={formikMain.handleBlur}
                      // sx={{
                      //   width: ["100%", "50%"], // Adjust width for different screen sizes
                      //   fontSize: ["14px", "16px"], // Adjust font size for different screen sizes
                      // }}
                    /> */}
                  </div>
                </div>
              </div>
            </div>

            <div className="flex flex-row gap-3 mt-">
              <div className="flex flex-row gap-3 mt-3"></div>
            </div>

            <Typography
              id="modal-modal-title"
              sx={{ fontWeight: "bold", marginTop: 2 }}
            >
              Agent's Information
            </Typography>
            {/* <div className="flex text-center justify-center mt-4 ">
              <Box className="flex flex-row bg-gray-100 w-[550px]  rounded-lg shadow-xl gap-[300px]">
                <Search>
                  <SearchIconWrapper>
                    <SearchIcon />
                  </SearchIconWrapper>
                  <StyledInputBase
                    //onClick={handleSearchModal}
                    //onClick={aboutToSearch}
                    placeholder="Type Agent"
                    // placeholder={searchType || "Search Name..."}
                    inputProps={{ "aria-label": "search" }}
                    value={searchValue}
                    onChange={(e) => handleChange(e.target.value)}
                  />
                </Search>
     
              </Box>
            </div> */}

            {/* <Button type="submit">Save Agent</Button> */}
            {/* </form> */}

            <div className="flex m-4 gap-4 ">
              <Button
                variant="contained"
                size="large"
                type="submit"
                sx={{ backgroundColor: "#242333" }}
                // onClick={handle_submit}
              >
                Save Record
              </Button>
              <Button
                onClick={handleClostRustModal}
                variant="contained"
                sx={{ backgroundColor: "#242333" }}
                size="large"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>

      <div className="relative sm:ml-[300px] ">
        <div className="flex flex-col relative bg-gradient-to-r from-gray-500 to-gray-800 w-full md:w-500px h-full overflow-hidden">
          <div className="relative m-6 flex flex-row gap-2">
            <div
              className="flex  flex-row gap-1 cursor-pointer hover:text-[#EE9B00] duration-150 ease-out"
              onClick={handleNav_Home}
            >
              <GoHome
                size={22}
                className="hover:text-[#EE9B00] duration-150 ease-out"
              />
              <h1 className="text-lg hover:text-[#EE9B00] duration-150 ease-out">
                Home
              </h1>
            </div>
            /
            <h2 className="text-lg text-[#EE9B00] font-semibold">GameSevers</h2>
          </div>
          <div className="flex flex-col sm:m-6 justify-center mt-12">
            <h1 className="text-base sm:text-sm md:text-lg lg:text-3xl font-bold text-[#f5f5f5] sm:ml-12 text-center">
              DISCOVER PREMIUM GAMESERVER HOSTING FROM WHTP
            </h1>
            <h1 className="text-base sm:text-md  text-[#f5f5f5] text-center p-2  mt-4 md:px-[100px] sm:px-[300px] px-[10px] font-bold">
              At WHTPG we offer premium game servers for over 90 different
              games, including many community favorites like Minecraft, ARK:
              Survival Evolved, Valheim and many more! Our servers use the best
              hardware and deliver a fantastic gaming experience with absolute
              top pings.
            </h1>
            <div className="flex sm:flex-row flex-col gap-4 m-2 p-3 justify-center w-full">
              <div className="flex flex-row justify-center gap-4">
                <span className="flex flex-row gap-1 hover:underline duration-200 ease-in cursor-pointer decoration-orange-500 underline-offset-8 decoration-4 ">
                  <FaPlus size={24} />
                  <h3 className="text-base sm:text-sm md:text-lg lg:text-xl">
                    OUR TOP GAMES
                  </h3>
                </span>
                <span className="flex flex-row gap-1 hover:underline cursor-pointer decoration-orange-500 underline-offset-8 decoration-4">
                  <MdGames size={24} />
                  <h3 className="text-base sm:text-sm md:text-lg lg:text-xl">
                    OUR TOP GAMES
                  </h3>
                </span>
              </div>
              <div>
                <Search sx={{ fontFamily: "Play" }}>
                  <SearchIconWrapper>
                    <SearchIcon />
                  </SearchIconWrapper>
                  <StyledInputBase
                    placeholder="Search for a game…"
                    inputProps={{ "aria-label": "search" }}
                  />
                </Search>
              </div>
            </div>
          </div>
          <div className="flex flex-col sm:flex-row  justify-start overflow-hidden ">
            <div
              className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
              style={{ backgroundImage: `url(${game12})` }}
            >
              <div className="flex relative top-[80%] justify-center">
                <button
                  className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest  sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in"
                  onClick={handleClickOrderNow}
                >
                  ORDER NOW
                </button>
              </div>
              <h1 className="flex relative top-[55%] justify-center font-bold">
                From $1.65
              </h1>
            </div>
            <div
              className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] m-4  sm:m-1 rounded-lg"
              style={{ backgroundImage: `url(${game8})` }}
            >
              <div className="flex relative top-[80%] justify-center">
                <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest  sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                  ORDER NOW
                </button>
              </div>
              <h1 className="flex relative top-[55%] justify-center font-bold">
                From $1.65
              </h1>
            </div>
            <div
              className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
              style={{ backgroundImage: `url(${game10})` }}
            >
              <div className="flex relative top-[80%] justify-center">
                <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                  ORDER NOW
                </button>
              </div>
              <h1 className="flex relative top-[55%] justify-center font-bold">
                From $1.65
              </h1>
            </div>
            <div
              className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
              style={{ backgroundImage: `url(${game11})` }}
            >
              <div className="flex relative top-[80%] justify-center">
                <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                  ORDER NOW
                </button>
              </div>
              <h1 className="flex relative top-[55%] justify-center font-bold">
                From $1.65
              </h1>
            </div>
            <div
              className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
              style={{ backgroundImage: `url(${game9})` }}
            >
              <div className="flex  relative top-[80%] justify-center">
                <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm  sm:tracking-wide  tracking-widest  sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                  ORDER NOW
                </button>
              </div>
              <h1 className="flex relative top-[55%] justify-center font-bold">
                From $1.65
              </h1>
            </div>
          </div>
          <div className="flex sm:flex-row flex-col justify-start overflow-hidden ">
            <div
              className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
              style={{ backgroundImage: `url(${game12})` }}
            >
              <div className="flex relative top-[80%] justify-center">
                <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest  sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                  ORDER NOW
                </button>
              </div>
              <h1 className="flex relative top-[55%] justify-center font-bold">
                From $1.65
              </h1>
            </div>
            <div
              className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] m-4  sm:m-1 rounded-lg"
              style={{ backgroundImage: `url(${game8})` }}
            >
              <div className="flex relative top-[80%] justify-center">
                <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest  sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                  ORDER NOW
                </button>
              </div>
              <h1 className="flex relative top-[55%] justify-center font-bold">
                From $1.65
              </h1>
            </div>
            <div
              className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
              style={{ backgroundImage: `url(${game10})` }}
            >
              <div className="flex relative top-[80%] justify-center">
                <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                  ORDER NOW
                </button>
              </div>
              <h1 className="flex relative top-[55%] justify-center font-bold">
                From $1.65
              </h1>
            </div>
            <div
              className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
              style={{ backgroundImage: `url(${game11})` }}
            >
              <div className="flex relative top-[80%] justify-center">
                <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm sm:tracking-wide tracking-widest sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                  ORDER NOW
                </button>
              </div>
              <h1 className="flex relative top-[55%] justify-center font-bold">
                From $1.65
              </h1>
            </div>
            <div
              className="bg-cover bg-center sm:w-[430px] sm:h-[256px] h-[300px] sm:m-1 m-4  rounded-lg"
              style={{ backgroundImage: `url(${game9})` }}
            >
              <div className="flex  relative top-[80%] justify-center">
                <button className="bg-[#EE9B00] text-gray-800 sm:text-xs text-sm  sm:tracking-wide  tracking-widest  sm:p-2 p-2 font-bold px-4 rounded-lg hover:scale-110 duration-200 ease-in">
                  ORDER NOW
                </button>
              </div>
              <h1 className="flex relative top-[55%] justify-center font-bold">
                From $1.65
              </h1>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const style_box = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  overflowY: "auto",
  overflowX: "auto",
  maxWidth: "80vh",
  maxHeight: "80vh",
  width: ["90%", "70%"],
};
